<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/', 'HomeController@home')->name('home');

Route::post('/search', 'HomeController@postSearch')->name('postSearch');
Route::get('/search', 'HomeController@home')->name('home');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::post('contact', 'HomeController@sendMessage')->name('sendMessage');
Route::get('faq', 'HomeController@faq')->name('faq');
Route::get('page/terms-of-services', 'HomeController@termsOfService')->name('termsOfService');
Route::get('page/privacy', 'HomeController@privacy')->name('privacy');

Route::get('user/{username}', 'HomeController@userProfile')->name('userProfile');
Route::get('user/password/reset', 'HomeController@showResetForm')->name('showResetForm');
Route::post('user/password/reset', 'HomeController@resetPassword')->name('resetPassword');
Route::get('user/password/change', 'HomeController@userChangePasswordFrom')->name('userChangePasswordFrom');
Route::get('email-verify/{hash}', 'HomeController@emailVerifyPage')->name('emailVerifyPage');




Route::get('filter/{type}/{payment}', 'HomeController@filterByPaymentMethod')->name('filterByPaymentMethod');

Route::get('buy-bitcoin', 'HomeController@home')->name('buyBitcoin');
Route::get('buy-litecoin', 'HomeController@home')->name('buyLitecoin');
Route::get('buy-dogecoin', 'HomeController@home')->name('buyDogecoin');

Route::get('sell-bitcoin', 'HomeController@home')->name('sellBitcoin');
Route::get('sell-litecoin', 'HomeController@home')->name('sellLitecoin');
Route::get('sell-dogecoin', 'HomeController@home')->name('sellDogecoin');
Route::get('btc-calculate-price/{amount}/{currency}/{ad_type}', 'HomeController@btcCalculatePrice')->name('btcCalculatePrice');

Route::get('btc-post-trade-msg', 'ChatsController@addTradeMsg')->name('addTradeMsg');
Route::post('upload-from-chat', 'ChatsController@uploadFromChat')->name('uploadFromChat');
Route::get('btc-check-new-file-messages', 'ChatsController@checkNewFileMessage')->name('checkNewFileMessage');
Route::get('btc-check-new-messages', 'ChatsController@checkNewMessage')->name('checkNewMessage');


Auth::routes();
Route::get('post-ad-view/{id}', 'HomeController@postAdView')->name('postAdView');


Route::group(['middleware' => 'auth'], function () {
	Route::get('logout',	'Auth\LoginController@logout')->name('member.logout');
	Route::get('post-ad', 'HomeController@postAd')->name('postAd');
	Route::post('post-ad', 'HomeController@storePostAd')->name('storePostAd');

	Route::get('account/advertisements', 'AdsController@advertisement')->name('account.advertisement');
	Route::get('account/advertisements/edit/{id}', 'AdsController@advertisementEdit')->name('account.advertisementEdit');
	Route::post('account/advertisements/update/{id}', 'AdsController@advertisementUpdate')->name('account.advertisementUpdate');
	Route::get('account/advertisements/delete/{id}', 'AdsController@advertisementDeleteConfirmation')->name('account.advertisementDeleteConfirmation');
	Route::get('account/advertisements/confirm/delete/{id}', 'AdsController@advertisementDelete')->name('account.advertisementDelete');

	Route::get('account/trades', 'TradesController@trade')->name('account.trade');
	Route::post('account/trade/make-trade', 'TradesController@tradeCreate')->name('account.tradeCreate');
	Route::get('account/trade/make-trade/{id}', 'TradesController@singleTrade')->name('account.singleTrade');
	Route::get('account/trade/cancel/{id}', 'TradesController@cancelTrade')->name('account.cancelTrade');
	Route::get('account/trade/report/{id}', 'TradesController@tradeReportForm')->name('account.tradeReportForm');
	Route::post('account/trade/report/{id}', 'TradesController@tradeReportSave')->name('account.tradeReportSave');
	Route::post('account/trade/process-trade', 'TradesController@processTrade')->name('account.processTrade');

	Route::get('leave-feedback/trade/{id}', 'TradesController@leaveFeedbackFrom')->name('leaveFeedbackFrom');
	Route::post('leave-feedback/trade/{id}', 'TradesController@leaveFeedbackSave')->name('leaveFeedbackSave');

	Route::get('account/wallet', 'AccountsController@wallet')->name('account.wallet');
	Route::post('account/wallet', 'AccountsController@walletSend')->name('account.walletSend');
	Route::get('account/transactions', 'AccountsController@transaction')->name('account.transaction');

	Route::get('account/settings', 'AccountsController@setting')->name('account.setting');
	Route::post('account/settings', 'AccountsController@changePassword')->name('account.setting');
	Route::get('account/verification', 'AccountsController@verification')->name('account.verification');
	Route::post('account/verification', 'AccountsController@doVerification')->name('account.doVerification');



});

	Route::get('cron/run', 'CronsController@cronJob')->name('cronJob');

