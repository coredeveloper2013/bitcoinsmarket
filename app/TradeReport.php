<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradeReport extends Model
{
    protected $table = 'trades_reports';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'trade_id',
        'content',
        'status',
        'time',
    ];

}
