<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'sms_code',
        'verified',
    ];

}
