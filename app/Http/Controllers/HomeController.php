<?php

namespace App\Http\Controllers;

use App\Faq;
use App\PostAdd;
use App\Trade;
use App\User;
use App\UsersRating;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    {
        $postAdds = PostAdd::all();

        $requestPath = $request->path();

        if($request->path() == '/'){
            $title = config('app.name', 'Home');
        }else{
            $title = ucwords(str_replace('-',' ',$requestPath));
        }
        return view('home')->with('title',$title)->with('coinTitle',$title)->with('postAdds',$postAdds);
    }

    public function filterByPaymentMethod(Request $request,$type,$paymentMethod)
    {
        $typeArr = explode('_',$type);
        if($typeArr[0] == 'buy'){
            $type = 'sell_'.$typeArr[1];
        }elseif($typeArr[0] == 'sell'){
            $type = 'buy_'.$typeArr[1];
        }
        $postAdds = PostAdd::where('type',$type)
            ->where('payment_method',$paymentMethod)
            ->get();

        if($request->path() == '/'){
            $title = config('app.name', 'Home');
        }else{
            $title = ucwords(str_replace('_',' ',$type));
        }
        return view('home')
            ->with('title',$title)
            ->with('coinTitle',$title)
            ->with('type',$type)
            ->with('postAdds',$postAdds)
            ->with('paymentMethod',$paymentMethod);
    }

    public function postSearch(Request $request)
    {
        if(is_null($request->amount)){
            $request->amount = 0;
        }
        $postAdds = PostAdd::where('min_amount','<=',ceil($request->amount))
            ->where('max_amount','>=',ceil($request->amount))
            ->where('type',$request->type)
            ->where('currency',$request->currency)
            ->where('payment_method',$request->payment_method)
            ->get();

        $requestPath = $request->path();

         if($request->path() == '/'){
            $title = config('app.name', 'Home');
        }else{
            $title = ucwords(str_replace('-',' ',$requestPath));
        }

        return view('search')->with('title',$title)->with('coinTitle',$title)->with('postAdds',$postAdds)->with('flag','search');
    }

    public function contact()
    {
        $title = 'Contact Us';
        return view('contact')->with('title',$title);
    }

    public function faq()
    {
        $title = 'FAQ';
        $model = Faq::all();
        return view('faq')->with('title',$title)->with('model',$model);
    }

    public function termsOfService()
    {
        $title = 'Terms of Services';
        return view('termsOfService')->with('title',$title);
    }

    public function privacy()
    {
        $title = 'Privacy Policy';
        return view('privacy')->with('title',$title);
    }
    public function postAd()
    {
        $title = 'Post Add';
        return view('post-ad')->with('title',$title);
    }

    public function storePostAd(Request $request)
    {
        $type = $request->get('type');

        $network = 'Bitcoin';
        if($type == "buy_litecoin" || $type == "sell_litecoin") {
            $network = 'Litecoin';
        }elseif($type == "buy_dogecoin" or $type == "sell_dogecoin"){
            $network = 'Dogecoin';
        }

        $require_document = 0;
        if($request->has('require_document')){
            $require_document = $request->get('require_document');
            if($require_document == 'yes'){
                $require_document = 1;
            }
        }

        $require_email = 0;
        if($request->has('require_email')){
            $require_email = $request->get('require_email');
            if($require_email == 'yes'){
                $require_email = 1;
            }
        }

        $require_mobile = 0;
        if($request->has('require_mobile')){
            $require_mobile = $request->get('require_mobile');
            if($require_mobile == 'yes'){
                $require_mobile = 1;
            }
        }

        $request->request->add([
            'uid' => Auth::user()->id,
            'network' => $network,
            'require_document' => $require_document,
            'require_email' => $require_email,
            'require_mobile' => $require_mobile,
        ]);

        $this->validator($request->all())->validate();
        $request->request->add(['price'=>$request->amount]);

        $model = PostAdd::create($request->all());

        $title = 'Post Ad';

        return redirect()->route('postAd')
                ->with('title',$title)
				->with('alert_class', 'success')
				->with('post_id', $model->id)
				->with('flash-message', __('crypto.success_3'));

    }

    public function postAdView($id)
    {
        $model = PostAdd::find($id);

        $title = 'View Post';
        return view('post-ad-view')
            ->with('title',$title)
            ->with('model',$model);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'uid' 				=> 'required',
			'type' 				=> 'required',
			'network' 			=> 'required',
			'payment_method' 	=> 'required',
			'currency' 			=> 'required',
			'payment_instructions' 	=> 'required',
			'amount' 			=> 'required|numeric',
			'min_amount' 			=> 'required',
			'max_amount' 			=> 'required',
			'process_time' 			=> 'required',
			'terms' 				=> 'required|min:3|max:150',
        ]);
    }

    public function emailVerifyPage($hash)
    {
        $title = 'Verify Email';
        $user = User::where('hash',$hash)->first();
        $status = false;
        if($user){
            $user->hash = '';
            $user->email_verified = 1;
            $user->save();
            $status = true;
        }
        return view('email-verify-page')->with('title',$title)->with('is_verify',$status);
    }

    public function userProfile($username)
    {
        $model = User::where('username',$username)->first();

        $advertisementsQuery = PostAdd::where('uid',$model->id)->first();
        $tradesQuery = Trade::where('uid',$model->id)->first();

        $rating = UsersRating::where('uid',$model->id)->get();

        $title = 'User Profile';
        return view('user')
            ->with('title',$title)
            ->with('model',$model)
            ->with('rating',$rating)
            ->with('totalAdd',count($advertisementsQuery))
            ->with('totalTrade',count($tradesQuery));
    }

    public function btcCalculatePrice($amount,$currency,$adType)
    {
       if($adType == "buy_bitcoin" or $adType == "sell_bitcoin") {
            if(is_numeric($amount)) {
                if($currency == "USD") {
                    $btcprice = get_current_bitcoin_price();
                    $com = $amount;
                    $com2 = ($btcprice * $com) / 100;
                    $amm = $btcprice - $com2;
                    echo 'Your Bitcoin price: '.ceil($amm).' '.$currency;
                } else {
                    $btcprice = get_current_bitcoin_price();
                    $com = $amount;
                    $com2 = ($btcprice * $com) / 100;
                    $com3 = $btcprice - $com2;
                    $amm = currencyConvertor($com3,"USD",$currency);
                    echo 'Your Bitcoin price: '.ceil($amm).' '.$currency.' ('.$com3.' USD)';
                }
            }
        } elseif($adType == "buy_litecoin" or  $adType == "sell_litecoin") {
            if(is_numeric($amount)) {
                if($currency == "USD") {
                    $btcprice = get_current_litecoin_price();
                    $com = $amount;
                    $com2 = ($btcprice * $com) / 100;
                    $amm = $btcprice - $com2;
                    echo 'Your Litecoin price: '.ceil($amm).' '.$currency;
                } else {
                    $btcprice = get_current_litecoin_price();
                    $com = $amount;
                    $com2 = ($btcprice * $com) / 100;
                    $com3 = $btcprice - $com2;
                    $amm = currencyConvertor($com3,"USD",$currency);
                    echo 'Your Litecoin price: '.ceil($amm).' '.$currency.' ('.$com3.' USD)';
                }
            }

        } elseif($adType == "buy_dogecoin" or $adType == "sell_dogecoin") {
            if(is_numeric($amount)) {
                if($currency == "USD") {
                    $btcprice = get_current_dogecoin_price();
                    $com = $amount;
                    $com2 = ($btcprice * $com) / 100;
                    $amm = $btcprice - $com2;
                    echo 'Your Dogecoin price: '.$amm.' '.$currency;
                } else {
                    $btcprice = get_current_dogecoin_price();
                    $com = $amount;
                    $com2 = ($btcprice * $com) / 100;
                    $com3 = $btcprice - $com2;
                    $amm = currencyConvertor($com3,"USD",$currency);
                    echo 'Your Dogecoin price: '.$amm.' '.$currency.' ('.$com3.' USD)';
                }
            }
        } else {
            echo 'Error';
        }

    }

    public function sendMessage(Request $request)
    {

        $this->validate($request, [
            'name'		    => 'required|min:3|max:32',
            'email' 		=> 'required|min:8|max:32|email',
            'subject' 		=> 'required|min:8|max:32',
            'message' 		=> 'required|min:3|max:255',
        ]);

            $msubject = '['.getSettingsData()->name.'] '.$request->get('subject');
            $mreceiver = getSettingsData()->supportemail;
            $headers = 'From: '.$mreceiver.'' . "\r\n" .
                'Reply-To: '.$request->get('email').'' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
            $mail = mail($mreceiver, $msubject, $request->message, $headers);

            $title = 'Contact';
            return redirect()->route('contact')
                ->with('title',$title)
				->with('alert_class', 'success')
				->with('flash-message', __('crypto.success_1'));

    }

    public function showResetForm()
    {
        $title = 'Reset Password';
        return view('account.reset')
            ->with('title',$title);

    }
    public function resetPassword(Request $request)
    {
        $email = $request->get('email');
        $hash = randomHash(15);
        $user = User::where('email',$email)->first();
        $title = 'Reset Password';
        if(count($user) == 1 && $user->email == $email){
            $user->hash = $hash;
            $user->save();
            emailsys_reset_password($user->id);

            return back()
                ->with('title',$title)
                ->with('alert_class', 'success')
                ->with('flash-message', __('crypto.success_11'));
        }else{
            return back()
                ->with('title',$title)
                ->with('alert_class', 'danger')
                ->with('flash-message', __('crypto.error_32'));
        }

        $title = 'Reset Password';
        return view('account.reset')
            ->with('title',$title);

    }



}
