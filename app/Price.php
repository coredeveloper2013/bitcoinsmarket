<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
//    protected $table = 'ads';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'type',
        'network',
        'payment_method',
        'currency',
        'payment_instructions',
        'price',
        'min_amount',
        'max_amount',
        'process_time',
        'terms',
        'require_document',
        'require_email',
        'require_mobile',
    ];

}
