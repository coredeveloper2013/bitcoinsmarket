<?php

use App\Price;
use App\Setting;
use App\TradeMessage;
use App\User;
use App\PostAdd;
use App\UsersRating;
use App\Trade;
use App\UsersAddress;
use App\BlockIoLicense;
use App\Helpers\BlockIo;
use App\UsersTransaction;

function randomHash($lenght = 7) {
	$random = substr(md5(rand()),0,$lenght);
	return $random;
}

function getSettingsData() {
     Return Setting::first();
}

function getTotalUser() {
     Return User::all()->count();
}

function getTotaladd() {
     Return PostAdd::all()->count();
}

function getTotalTrade() {
     Return Trade::all()->count();
}

function getUserInfo($id) {
     Return User::find($id);
}

function getBuyBitcoinAdd(){
    Return PostAdd::where('type','buy_bitcoin')->where('network','Bitcoin')->inRandomOrder()->take(10)->get();
}
function getBuyLitecoinAdd(){
    Return PostAdd::where('type','buy_litecoin')->where('network','LiteCoin')->inRandomOrder()->take(10)->get();
}
function getBuyDogicoinAdd(){
    Return PostAdd::where('type','buy_dogecoin')->where('network','Dogecoin')->inRandomOrder()->take(10)->get();
}

function getSellBitcoinAdd(){
    Return PostAdd::where('type','sell_bitcoin')->where('network','Bitcoin')->inRandomOrder()->take(10)->get();
}
function getSellLitecoinAdd(){
    Return PostAdd::where('type','sell_litecoin')->where('network','LiteCoin')->inRandomOrder()->take(10)->get();
}
function getSellDogicoinAdd(){
    Return PostAdd::where('type','sell_dogecoin')->where('network','Dogecoin')->inRandomOrder()->take(10)->get();
}

function get_current_bitcoin_price() {
     $model =  Price::where('currency','USD')->where('network','Bitcoin')->orderBy('price', 'desc')->first();
     if($model){
         return $model->price;
     }
}

function get_current_litecoin_price() {
    $model = Price::where('currency','USD')->where('network','Litecoin')->orderBy('price', 'desc')->first();
    if($model){
         return $model->price;
     }
}

function get_current_dogecoin_price() {
    $model = Price::where('currency','EUR')->where('network','Dogecoin')->orderBy('price', 'desc')->first();
    if($model){
        $amount = urlencode($model->price);
        $from_Currency = urlencode($from_Currency);
		$to_Currency = urlencode($to_Currency);
		$get = file_get_contents("https://$from_Currency.mconvert.net/$to_Currency/$amount");
		$get = explode('<span class="convert-result result">',$result);
		$get = explode("</span>",$get[1]);
		$converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
		return $converted_amount;
    }else{
        return null;
    }
}


function generateBtcLtcDogeAddressApi($username,$network) {

    $blockIoLicense = BlockIoLicense::where('network',$network)
        ->where('default_license',1)
        ->orderBy('id')
        ->first();

    if($blockIoLicense){
        $user = User::where('username',$username)->first();

        $label = 'usr_'.$username;
        $apiKey = $blockIoLicense->license;
		$pin = $blockIoLicense->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);
		$new_address = $block_io->get_new_address(['label' => $label]);
		if($new_address->status == "success") {
			$addr = $new_address->data->address;
			$time = time();
			UsersAddress::create([
			    'uid' => $user->id,
			    'network' => $network,
			    'label' => $label,
			    'address' => $addr,
			    'lid' => $blockIoLicense->id,
			    'available_balance' => '0.00000000',
			    'pending_received_balance' => '0.00000000',
			    'status' => '1',
			    'created' => $time,
            ]);
			$blockIoLicense->addresses +=1;
			$blockIoLicense->save();
		}
    }
}

function is_online($uid) {
	$currenttime = time()-300;
	$onlinetime = getUserInfo($uid)->time_activity;
	if($onlinetime > $currenttime) {
		return 1;
	} else {
		return 0;
	}
}

function update_activity($uid) {

	$time = time();
	User::where('id',$uid)->update(['time_activity'=>$time]);
}

function activity_time($uid) {
    $time = time();
	$currenttime = $time-300;
	$onlinetime = getUserInfo($uid)->time_activity;
	if($onlinetime > $currenttime) {
		return 'Is online';
	} else {
		return 'Last seen '.timeago($onlinetime);
	}
}

function timeago($time)
{
   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
   $lengths = array("60","60","24","7","4.35","12","10");

   $now = time();

       $difference     = $now - $time;
       $tense         = "ago";

   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }

   $difference = round($difference);

   if($difference != 1) {
       $periods[$j].= "s";
   }

   return "$difference $periods[$j] ago ";
}


function getFeedback($uid,$type){
    $userRating = UsersRating::where('uid',$uid)->where('type',$type)->get();
    return count($userRating);
}


function convertBTCprice($amount,$currency) {
	if(is_numeric($amount)) {
		if($currency == "USD") {
			$btcprice = get_current_bitcoin_price();
			$com = $amount;
			$com2 = ($btcprice * $com) / 100;
			$amm = $btcprice - $com2;
			return number_format($amm, 2, '.', '');
		} else {
			$btcprice = get_current_bitcoin_price();
			$com = $amount;
			$com2 = ($btcprice * $com) / 100;
			$com3 = $btcprice - $com2;
			$amm = currencyConvertor($com3,"USD",$currency);
			return number_format($amm, 2, '.', '');
		}
	}
}

function convertLTCprice($amount,$currency) {
	if(is_numeric($amount)) {
		if($currency == "USD") {
			$ltcprice = get_current_litecoin_price();
			$com = $amount;
			$com2 = ($ltcprice * $com) / 100;
			$amm = $ltcprice - $com2;
			return ceil($amm);
		} else {
			$ltcprice = get_current_litecoin_price();
			$com = $amount;
			$com2 = ($ltcprice * $com) / 100;
			$com3 = $ltcprice - $com2;
			$amm = currencyConvertor($com3,"USD",$currency);
			return ceil($amm);
		}
	}
}

function convertDOGEprice($amount,$currency) {
	if(is_numeric($amount)) {
		if($currency == "USD") {
			$dogeprice = get_current_dogecoin_price();
			$com = $amount;
			$com2 = ($dogeprice * $com) / 100;
			$amm = $dogeprice - $com2;
			return $amm;
		} else {
			$dogeprice = get_current_dogecoin_price();
			$com = $amount;
			$com2 = ($dogeprice * $com) / 100;
			$com3 = $dogeprice - $com2;
			$amount = urlencode($com3);
			  $from_Currency = urlencode($from_Currency);
			  $to_Currency = urlencode($currency);
			  $get = file_get_contents("https://$from_Currency.mconvert.net/$to_Currency/$amount");
			  $get = explode('<span class="convert-result result">',$result);
				$get = explode("</span>",$get[1]);
			  $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
			return $converted_amount;
		}
	}
}
function currencyConvertor($amount,$from_Currency,$to_Currency) {
	$amount = urlencode($amount);
	$from_Currency = urlencode(strtolower($from_Currency));
	$to_Currency = urlencode(strtolower($to_Currency));
	// $get = "https://finance.google.co.uk/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";
	$get = "https://$from_Currency.mconvert.net/$to_Currency/$amount";
	$ch = curl_init();
	$url = $get;
	// Disable SSL verification
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// Will return the response, if false it print the response
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// Set the url
	curl_setopt($ch, CURLOPT_URL,$url);
	// Execute
	$result=curl_exec($ch);

	$error_code = curl_errno($ch);

	// Closing
	curl_close($ch);
	if($error_code > 0){
	    return number_format(0, 2, '.', '');
    }
	$get = explode('<span class="convert-result result">',$result);
	$get = explode("</span>",$get[1]);
	//return  print_r($get);
	$get[0] = str_ireplace(",",".",$get[0]);
	$converted_amount = preg_replace("/[^0-9\.,]/", null, $get[0]);
	return number_format($converted_amount, 2, '.', '');
}

function tradeinfo($tid,$value) {
	$model =  Trade::where('id',$tid)->first();
	if($model){
	    return $model->$value;
    }else{
	    return NULL;
    }
}

function adinfo($aid,$value) {
	$model =  PostAdd::where('id',$aid)->first();
	if($model){
	    return $model->$value;
    }else{
	    return NULL;
    }
}

function walletinfo($uid,$value,$network) {
	$model =  UsersAddress::where('uid',$uid)
        ->where('network',$network)->first();
	if($model){
	    return $model->$value;
    }else{
	    return NULL;
    }
}


function get_user_balance($uid,$network) {
    $balance = walletinfo($uid,'available_balance',$network);

	$model = Trade::where('uid',$uid)
        ->where('network',$network)
        ->where('status','<',3)
        ->where('type','LIKE','%sell_%')->get();

	if($model){
	    foreach ($model as $single){
	        $balance = $balance-$single->crypto_amount;
	        if($balance == 'Bitcoin'){
	            $balance = $balance-0.0009;
				$balance = $balance-getSettingsData()->bitcoin_sell_comission;
            }elseif($balance == 'Litecoin'){
                $balance = $balance-0.005;
				$balance = $balance-getSettingsData()->litecoin_sell_comission;
            }elseif($balance == 'Dogecoin'){
	            $balance = $balance-4;
				$balance = $balance-getSettingsData()->dogecoin_sell_comission;
            }else{

            }
        }
    }

   $model = Trade::where('trader',$uid)
        ->where('network',$network)
        ->where('status','<',3)
        ->where('type','LIKE','%buy_%')->get();

   if($model){
	    foreach ($model as $single){
	        $balance = $balance-$single->crypto_amount;
	        if($balance == 'Bitcoin'){
	            $balance = $balance-0.0009;
				$balance = $balance-getSettingsData()->bitcoin_buy_comission;
            }elseif($balance == 'Litecoin'){
                $balance = $balance-0.005;
				$balance = $balance-getSettingsData()->litecoin_buy_comission;
            }elseif($balance == 'Dogecoin'){
	            $balance = $balance-4;
				$balance = $balance-getSettingsData()->dogecoin_buy_comission;
            }else{

            }
        }
   }

   if($network == "Dogecoin") {
       return $balance;
   }else{
       return number_format($balance,8);
   }
}

function success($text) {
	return '<div class="alert alert-success"><i class="fa fa-check"></i> '.$text.'</div>';
}

function error($text) {
	return '<div class="alert alert-danger"><i class="fa fa-times"></i> '.$text.'</div>';
}



function getTradesMsg($tradeId){
    return TradeMessage::where('trade_id',$tradeId)->orderBy('id','DESC')->get();
}

function readTradeMsg($tradeId){
    TradeMessage::find($tradeId)->update(['readed'=>1]);
}

function checkFeedback($trade_id){
    $loginUser = Auth::user();
    Return UsersRating::where('trade_id',$trade_id)->where('author',$loginUser->id)->count();
}

function getVerifyType() {

	if(getSettingsData()->document_verification == "1" && getSettingsData()->email_verification == "1" && getSettingsData()->phone_verification == "1") {
		$status = '1';
	} elseif(getSettingsData()->document_verification == "1" && getSettingsData()->email_verification == "1" && getSettingsData()->phone_verification == "0") {
		$status = '2';
	} elseif(getSettingsData()->document_verification == "1" && getSettingsData()->email_verification == "0" && getSettingsData()->phone_verification == "1") {
		$status = '3';
	} elseif(getSettingsData()->document_verification == "0" && getSettingsData()->email_verification == "1" && getSettingsData()->phone_verification == "1") {
		$status = '4';
	} elseif(getSettingsData()->document_verification == "1" && getSettingsData()->email_verification == "1" && getSettingsData()->phone_verification == "0") {
		$status = '5';
	} elseif(getSettingsData()->document_verification == "1" && getSettingsData()->email_verification == "0" && getSettingsData()->phone_verification == "0") {
		$status = '6';
	} elseif(getSettingsData()->document_verification == "0" && getSettingsData()->email_verification == "1" && getSettingsData()->phone_verification == "0") {
		$status = '7';
	} elseif(getSettingsData()->document_verification == "0" && getSettingsData()->email_verification == "0" && getSettingsData()->phone_verification == "1") {
		$status = '8';
	} elseif(getSettingsData()->document_verification == "0" && getSettingsData()->email_verification == "0" && getSettingsData()->phone_verification == "0") {
		$status = '9';
	} else {
		$status = '0';
	}
	return $status;
}

function verifiedUser(){
    $loginUser = Auth::user();
    $loginUser->status = 3;
    $loginUser->save();
}

function emailsys_verification_email($id) {
	$user = getUserInfo($id);

	if(count($user)>0) {
		$settingsData = getSettingsData();
		$email = $user->email;
		$msubject = '['.$settingsData->name.'] Account verification';
		$mreceiver = $email;
		$message = 'Hello, '.$user->username.'
		
To unlock '.$settingsData->name.' full features need to activate your account with link below.
'.$settingsData->url.'email-verify/'.$user->hash.'
	
If you have some problems please feel free to contact with us on '.$settingsData->supportemail;
		$headers = 'From: '.$settingsData->infoemail.'' . "\r\n" .
					'Reply-To: '.$settingsData->infoemail.'' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();
		$mail = mail($mreceiver, $msubject, $message, $headers);
		if($mail) {
			return true;
		} else {
			return false;
		}
	}
}

function emailsys_reset_password($uid) {

    $user = User::find($uid);
	if(count($user)>0) {

		$email = $user->email;
		$msubject = '['.getSettingsData()->name.'] Reset password request';
		$mreceiver = $email;
		$message = 'Hello, '.$user->username.'
		
You use form to reset password in our website. To change it click on link below.
'.getSettingsData()->url.'/user/password/change/'.$user->hash.'

If this email was not requested by you, please ignore it.
	
If you have some problems please feel free to contact with us on '.getSettingsData()->supportemail;
		$headers = 'From: '.getSettingsData()->infoemail.'' . "\r\n" .
					'Reply-To: '.getSettingsData()->infoemail.'' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();
		$mail = mail($mreceiver, $msubject, $message, $headers);
		if($mail) {
			return true;
		} else {
			return false;
		}
	}
}

function btc_update_balance($uid) {
    $usersAddress = UsersAddress::where('uid',$uid)
        ->where('network','Bitcoin')
        ->first();

    if(count($usersAddress)>0){
        $licence = BlockIoLicense::find($usersAddress->lid);

        $user_address = $licence->address;
		$apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);

		try{
            $balance = $block_io->get_address_balance(array('addresses' => $user_address));

            if($balance->status == "success") {
                $time = time();
                $available_balance = $balance->data->available_balance;
                $pending_received_balance = $balance->data->pending_received_balance;
                $usersAddress->available_balance = $available_balance;
                $usersAddress->pending_received_balance = $pending_received_balance;
                $usersAddress->save();
            }
        }catch (\Exception $e){
            echo $e->getMessage();
            exit();
        }
    }
}

function ltc_update_balance($uid) {
    $usersAddress = UsersAddress::where('uid',$uid)
        ->where('network','Litecoin')
        ->first();

    if(count($usersAddress)>0){
        $licence = BlockIoLicense::find($usersAddress->lid);

        $user_address = $licence->address;
		$apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);

		try{
            $balance = $block_io->get_address_balance(array('addresses' => $user_address));

            if($balance->status == "success") {
                $time = time();
                $available_balance = $balance->data->available_balance;
                $pending_received_balance = $balance->data->pending_received_balance;
                $usersAddress->available_balance = $available_balance;
                $usersAddress->pending_received_balance = $pending_received_balance;
                $usersAddress->save();
            }
        }catch (\Exception $e){
            echo $e->getMessage();
            exit();
        }
    }
}

function doge_update_balance($uid) {
    $usersAddress = UsersAddress::where('uid',$uid)
        ->where('network','Dogecoin')
        ->first();

    if(count($usersAddress)>0){
        $licence = BlockIoLicense::find($usersAddress->lid);

        $user_address = $licence->address;
		$apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);

		try{
            $balance = $block_io->get_address_balance(array('addresses' => $user_address));

            if($balance->status == "success") {
                $time = time();
                $available_balance = $balance->data->available_balance;
                $pending_received_balance = $balance->data->pending_received_balance;
                $usersAddress->available_balance = $available_balance;
                $usersAddress->pending_received_balance = $pending_received_balance;
                $usersAddress->save();
            }
        }catch (\Exception $e){
            echo $e->getMessage();
            exit();
        }
    }
}

function StdClass2array($class)
{
    $array = array();

    foreach ($class as $key => $item)
    {
            if ($item instanceof StdClass) {
                    $array[$key] = StdClass2array($item);
            } else {
                    $array[$key] = $item;
            }
    }

    return $array;
}


function btc_update_transactions($uid) {
	$usersAddress = UsersAddress::where('uid',$uid)
        ->where('network','Bitcoin')
        ->first();

    if(count($usersAddress)>0){
        $licence = BlockIoLicense::find($usersAddress->lid);

		$apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);

        $received = $block_io->get_transactions(array('type' => 'received', 'addresses' => $usersAddress->address));

        if($received->status == "success") {
            $data = $received->data->txs;
            $dt = StdClass2array($data);

            foreach($dt as $k=>$v) {
                $txid = $v['txid'];
                $time = $v['time'];
                $amounts = $v['amounts_received'];
                $amounts = StdClass2array($amounts);
                foreach($amounts as $a => $b) {
                    $recipient = $b['recipient'];
                    $amount = $b['amount'];
                }
                $senders = $v['senders'];
                $senders = StdClass2array($senders);
                foreach($senders as $c => $d) {
                     $sender = $d;
                }
                $confirmations = $v['confirmations'];
                $check = UsersTransaction::where('uid',$uid)
                    ->where('txid',$txid)
                    ->first();

                if(count($check) > 0) {
                    $check->confirmations = $confirmations;
                    $check->save();

                } else {
                    UsersTransaction::create([
                        'uid' => $uid,
                        'network' => 'Bitcoin',
                        'type' => 'received',
                        'recipient' => $recipient,
                        'sender' => $sender,
                        'amount' => $amount,
                        'time' => $time,
                        'confirmations' => $confirmations,
                        'txid' => $txid,
                    ]);
                }
            }
        }

        $sent = $block_io->get_transactions(array('type' => 'sent', 'addresses' => $usersAddress->address));

        if($sent->status == "success") {
            $data = $sent->data->txs;
            $dt = StdClass2array($data);
            foreach ($dt as $k => $v) {
                $txid = $v['txid'];
                $time = $v['time'];
                $amounts = $v['amounts_sent'];
                $amounts = StdClass2array($amounts);
                foreach ($amounts as $a => $b) {
                    $recipient = $b['recipient'];
                    $amount = $b['amount'];
                }
                $senders = $v['senders'];
                $senders = StdClass2array($senders);
                foreach ($senders as $c => $d) {
                    $sender = $d;
                }
                $confirmations = $v['confirmations'];
                $check = UsersTransaction::where('uid', $uid)
                    ->where('txid', $txid)
                    ->first();

                if (count($check) > 0) {
                    $check->confirmations = $confirmations;
                    $check->save();

                } else {
                    UsersTransaction::create([
                        'uid' => $uid,
                        'network' => 'Bitcoin',
                        'type' => 'sent',
                        'recipient' => $recipient,
                        'sender' => $sender,
                        'amount' => $amount,
                        'time' => $time,
                        'confirmations' => $confirmations,
                        'txid' => $txid,
                    ]);
                }
            }
        }
	}
}

function ltc_update_transactions($uid) {
	$usersAddress = UsersAddress::where('uid',$uid)
        ->where('network','Litecoin')
        ->first();

    if(count($usersAddress)>0){
        $licence = BlockIoLicense::find($usersAddress->lid);

		$apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);

        $received = $block_io->get_transactions(array('type' => 'received', 'addresses' => $usersAddress->address));

        if($received->status == "success") {
            $data = $received->data->txs;
            $dt = StdClass2array($data);

            foreach($dt as $k=>$v) {
                $txid = $v['txid'];
                $time = $v['time'];
                $amounts = $v['amounts_received'];
                $amounts = StdClass2array($amounts);
                foreach($amounts as $a => $b) {
                    $recipient = $b['recipient'];
                    $amount = $b['amount'];
                }
                $senders = $v['senders'];
                $senders = StdClass2array($senders);
                foreach($senders as $c => $d) {
                     $sender = $d;
                }
                $confirmations = $v['confirmations'];
                $check = UsersTransaction::where('uid',$uid)
                    ->where('txid',$txid)
                    ->first();

                if(count($check) > 0) {
                    $check->confirmations = $confirmations;
                    $check->save();

                } else {
                    UsersTransaction::create([
                        'uid' => $uid,
                        'network' => 'Litecoin',
                        'type' => 'received',
                        'recipient' => $recipient,
                        'sender' => $sender,
                        'amount' => $amount,
                        'time' => $time,
                        'confirmations' => $confirmations,
                        'txid' => $txid,
                    ]);
                }
            }
        }

        $sent = $block_io->get_transactions(array('type' => 'sent', 'addresses' => $usersAddress->address));

        if($sent->status == "success") {
            $data = $sent->data->txs;
            $dt = StdClass2array($data);
            foreach ($dt as $k => $v) {
                $txid = $v['txid'];
                $time = $v['time'];
                $amounts = $v['amounts_sent'];
                $amounts = StdClass2array($amounts);
                foreach ($amounts as $a => $b) {
                    $recipient = $b['recipient'];
                    $amount = $b['amount'];
                }
                $senders = $v['senders'];
                $senders = StdClass2array($senders);
                foreach ($senders as $c => $d) {
                    $sender = $d;
                }
                $confirmations = $v['confirmations'];
                $check = UsersTransaction::where('uid', $uid)
                    ->where('txid', $txid)
                    ->first();

                if (count($check) > 0) {
                    $check->confirmations = $confirmations;
                    $check->save();

                } else {
                    UsersTransaction::create([
                        'uid' => $uid,
                        'network' => 'Litecoin',
                        'type' => 'sent',
                        'recipient' => $recipient,
                        'sender' => $sender,
                        'amount' => $amount,
                        'time' => $time,
                        'confirmations' => $confirmations,
                        'txid' => $txid,
                    ]);
                }
            }
        }

	}
}

function doge_update_transactions($uid) {
	$usersAddress = UsersAddress::where('uid',$uid)
        ->where('network','Dogecoin')
        ->first();

    if(count($usersAddress)>0){
        $licence = BlockIoLicense::find($usersAddress->lid);

		$apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);

        $received = $block_io->get_transactions(array('type' => 'received', 'addresses' => $usersAddress->address));

        if($received->status == "success") {
            $data = $received->data->txs;
            $dt = StdClass2array($data);

            foreach($dt as $k=>$v) {
                $txid = $v['txid'];
                $time = $v['time'];
                $amounts = $v['amounts_received'];
                $amounts = StdClass2array($amounts);
                foreach($amounts as $a => $b) {
                    $recipient = $b['recipient'];
                    $amount = $b['amount'];
                }
                $senders = $v['senders'];
                $senders = StdClass2array($senders);
                foreach($senders as $c => $d) {
                     $sender = $d;
                }
                $confirmations = $v['confirmations'];
                $check = UsersTransaction::where('uid',$uid)
                    ->where('txid',$txid)
                    ->first();

                if(count($check) > 0) {
                    $check->confirmations = $confirmations;
                    $check->save();

                } else {
                    UsersTransaction::create([
                        'uid' => $uid,
                        'network' => 'Dogecoin',
                        'type' => 'received',
                        'recipient' => $recipient,
                        'sender' => $sender,
                        'amount' => $amount,
                        'time' => $time,
                        'confirmations' => $confirmations,
                        'txid' => $txid,
                    ]);
                }
            }
        }

        $sent = $block_io->get_transactions(array('type' => 'sent', 'addresses' => $usersAddress->address));

        if($sent->status == "success") {
            $data = $sent->data->txs;
            $dt = StdClass2array($data);
            foreach ($dt as $k => $v) {
                $txid = $v['txid'];
                $time = $v['time'];
                $amounts = $v['amounts_sent'];
                $amounts = StdClass2array($amounts);
                foreach ($amounts as $a => $b) {
                    $recipient = $b['recipient'];
                    $amount = $b['amount'];
                }
                $senders = $v['senders'];
                $senders = StdClass2array($senders);
                foreach ($senders as $c => $d) {
                    $sender = $d;
                }
                $confirmations = $v['confirmations'];
                $check = UsersTransaction::where('uid', $uid)
                    ->where('txid', $txid)
                    ->first();

                if (count($check) > 0) {
                    $check->confirmations = $confirmations;
                    $check->save();

                } else {
                    UsersTransaction::create([
                        'uid' => $uid,
                        'network' => 'Dogecoin',
                        'type' => 'sent',
                        'recipient' => $recipient,
                        'sender' => $sender,
                        'amount' => $amount,
                        'time' => $time,
                        'confirmations' => $confirmations,
                        'txid' => $txid,
                    ]);
                }
            }
        }

	}
}

function btc_delete_fee_transactions($uid) {

    $usersAddress = UsersAddress::where('uid',$uid)
        ->where('network','Bitcoin')
        ->first();

    if(count($usersAddress)>0){
        $licence = BlockIoLicense::find($usersAddress->lid);

        $addr = $licence->address;
		$apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);

		$usersTrxns = UsersTransaction::where('uid', $uid)
            ->where('type', 'sent')
            ->get();
		if(count($usersTrxns)>0){
		    foreach ($usersTrxns as $single){
		        if($licence->address == $single->recipient){
		            UsersTransaction::where('id',$single->id)
                        ->where('uid',$uid)->delete();
                }
            }
        }
    }
}

function ltc_delete_fee_transactions($uid) {

    $usersAddress = UsersAddress::where('uid',$uid)
        ->where('network','Litecoin')
        ->first();

    if(count($usersAddress)>0){
        $licence = BlockIoLicense::find($usersAddress->lid);

        $addr = $licence->address;
		$apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);

		$usersTrxns = UsersTransaction::where('uid', $uid)
            ->where('type', 'sent')
            ->get();
		if(count($usersTrxns)>0){
		    foreach ($usersTrxns as $single){
		        if($licence->address == $single->recipient){
		            UsersTransaction::where('id',$single->id)
                        ->where('uid',$uid)->delete();
                }
            }
        }
    }
}

function doge_delete_fee_transactions($uid) {

    $usersAddress = UsersAddress::where('uid',$uid)
        ->where('network','Dogecoin')
        ->first();

    if(count($usersAddress)>0){
        $licence = BlockIoLicense::find($usersAddress->lid);

        $addr = $licence->address;
		$apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);

		$usersTrxns = UsersTransaction::where('uid', $uid)
            ->where('type', 'sent')
            ->get();
		if(count($usersTrxns)>0){
		    foreach ($usersTrxns as $single){
		        if($licence->address == $single->recipient){
		            UsersTransaction::where('id',$single->id)
                        ->where('uid',$uid)->delete();
                }
            }
        }
    }
}



function btc_get_bitcoin_prices() {
    $licence = BlockIoLicense::where('default_license',1)
        ->where('network','Bitcoin')
        ->first();

    if(count($licence)>0){
        $apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);
        try{
            $price = $block_io->get_current_price();
        }catch (\Exception $e){
            $errorMsg = $e->getMessage();
        }

		if(isset($price->status) && $price->status == "success") {
		    $prices = $price->data->prices;
            $prices = StdClass2array($prices);

            foreach($prices as $k => $v) {
                foreach($v as $a => $b) {
                    $rows[$a] = $b;
                }
                $proceModel = Price::where('source',$rows['exchange'])
                    ->where('currency',$rows['price_base'])
                    ->where('network','Bitcoin')
                    ->first();

                if(count($proceModel)>0){
                    $proceModel->$rows['price'];
                    $proceModel->save();
                }else{
                    Price::create([
                        'source' => $rows['exchange'],
                        'network' => 'Bitcoin',
                        'price' => $rows['price'],
                        'currency' => $rows['price_base'],
                    ]);
                }
            }
        }else{
            return $errorMsg;
        }
    }
}

function ltc_get_litecoin_prices() {
    $licence = BlockIoLicense::where('default_license',1)
        ->where('network','Litecoin')
        ->first();

    if(count($licence)>0){
        $apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);
        try{
            $price = $block_io->get_current_price();
        }catch (\Exception $e){
            $errorMsg = $e->getMessage();
        }

		if(isset($price->status) && $price->status == "success") {
		    $prices = $price->data->prices;
            $prices = StdClass2array($prices);

            foreach($prices as $k => $v) {
                foreach($v as $a => $b) {
                    $rows[$a] = $b;
                }
                $proceModel = Price::where('source',$rows['exchange'])
                    ->where('currency',$rows['price_base'])
                    ->where('network','Litecoin')
                    ->first();

                if(count($proceModel)>0){
                    $proceModel->$rows['price'];
                    $proceModel->save();
                }else{
                    Price::create([
                        'source' => $rows['exchange'],
                        'network' => 'Litecoin',
                        'price' => $rows['price'],
                        'currency' => $rows['price_base'],
                    ]);
                }
            }
        }else{
            return $errorMsg;
        }
    }
}

function doge_get_dogecoin_prices() {
    $licence = BlockIoLicense::where('default_license',1)
        ->where('network','Dogecoin')
        ->first();

    if(count($licence)>0){
        $apiKey = $licence->license;
		$pin = $licence->secret_pin;
		$version = 2; // the API version
		$block_io = new BlockIo($apiKey, $pin, $version);
        try{
            $price = $block_io->get_current_price();
        }catch (\Exception $e){
            $errorMsg = $e->getMessage();
        }

		if(isset($price->status) && $price->status == "success") {
		    $prices = $price->data->prices;
            $prices = StdClass2array($prices);

            foreach($prices as $k => $v) {
                foreach($v as $a => $b) {
                    $rows[$a] = $b;
                }
                $proceModel = Price::where('source',$rows['exchange'])
                    ->where('currency',$rows['price_base'])
                    ->where('network','Dogecoin')
                    ->first();

                if(count($proceModel)>0){
                    $proceModel->$rows['price'];
                    $proceModel->save();
                }else{
                    Price::create([
                        'source' => $rows['exchange'],
                        'network' => 'Dogecoin',
                        'price' => $rows['price'],
                        'currency' => $rows['price_base'],
                    ]);
                }
            }
        }else{
            return $errorMsg;
        }
    }
}


function btc_check_expired_trades() {
	$time = time();
	$trades = Trade::where('status','<',3)
        ->where('timeout','<',$time)
        ->get();
	if(count($trades)>0){
	    foreach ($trades as $single){
	        $single->status = 6;
	        $single->save();
        }
    }
}



