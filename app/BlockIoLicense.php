<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockIoLicense extends Model
{
   protected $table = 'blockio_licenses';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account',
        'network',
        'license',
        'secret_pin',
        'address',
        'addresses',
        'default_license',
    ];

}
