<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersTransaction extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'network',
        'type',
        'recipient',
        'sender',
        'amount',
        'time',
        'confirmations',
        'txid',
    ];

}
