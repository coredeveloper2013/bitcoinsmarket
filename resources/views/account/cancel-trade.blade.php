@extends('layouts.master')
@section('content')

    <div class="container-fluid page-title">
			<div class="row blue-banner">
            	<div class="container main-container">
                	<div class="col-lg-12 col-md-12 col-sm-12">
                		<h3 class="white-heading">Cancel Trade</h3>
                    </div>
                </div>
            </div>
        </div>
  	 <!--header section -->
	 <!-- full width section -->
    	<div class="container-fluid white-bg">
        	<div class="row">
            	<div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">

								<div class="alert alert-info"><i class="fa fa-check"></i> {{$msg}}</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	<script type="text/javascript">
		function redirect() {
			window.location.href='{{route('account.trade')}}';
		}
		setTimeout(redirect,5000);
		</script>

@endsection