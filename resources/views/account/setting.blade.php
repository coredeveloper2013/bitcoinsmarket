@extends('layouts.master')
@section('content')
    
     <!-- Page Title-->
    	<div class="container-fluid blue-banner page-title bg-image">
		 
        </div>
    <!-- Page Title-->
	<div class="container ex_padding" style="padding-top:20px;padding-bottom:20px;font-size:15px;">
		<div class="row">
			<div class="col-md-3">
				
				@include('account.accordion-menu')
				
			</div>
			<div class="col-md-9">
			
				<div class="panel panel-default">
					<div class="panel-body">
						<h4>@lang('crypto.menu_settings')</h4>
						<hr/>

                        @include('flash-message')

						<form action="{{route('account.setting')}}" method="POST">
                            {{csrf_field()}}
							<div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
								<label>@lang('crypto.current_password')</label>
								<input type="password" class="form-control" name="old_password">
                                 @if ($errors->has('old_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                @endif
							</div>
							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label>@lang('crypto.new_password')</label>
								<input type="password" class="form-control" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
							</div>	
							<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
								<label>@lang('crypto.confirm_password')</label>
								<input type="password" class="form-control" name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
							</div>
							<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> @lang('crypto.btn_change_password')</button>
						</form>
					</div>
				</div>
			
			</div>
		</div>
	</div>
    
@endsection