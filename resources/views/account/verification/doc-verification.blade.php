@if(!empty($uploadStatus))
    <div class="alert alert-success"><i class="fa fa-check"></i> @lang('crypto.success_16')</div>
@endif
<h4>@lang('crypto.document_verification')</h4>
<hr/>
@if(getUserInfo($loginUser->id)->document_verified)
    <p><span class="text text-success"><i class="fa fa-check"></i> @lang('crypto.documents_accepted')</span></p>
@else
    @if(getUserInfo($loginUser->id)->document_1)
        <p><span class="text text-info"><i class="fa fa-clock-o"></i> @lang('crypto.documents_awaiting_review')</span></p>
    @else
        <form action="{{route('account.doVerification')}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label>@lang('crypto.scanned_passpord')</label>
                <input type="file" class="form-control" name="document_1">
            </div>
            <div class="form-group">
                <label>@lang('crypto.scanned_invoice')</label>
                <input type="file" class="form-control" name="document_2">
            </div>
            <input type="hidden" name="verify" value="doc">
            <button type="submit" class="btn btn-primary btn-sm" ><i class="fa fa-upload"></i> @lang('crypto.btn_upload_files')</button>
        </form>
    @endif
@endif
<br>