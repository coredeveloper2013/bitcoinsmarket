@if(!empty($emailStatus))
    <div class="alert alert-success"><i class="fa fa-check"></i> @lang('crypto.success_15')</div>
@endif

<h4>@lang('crypto.email_verification')</h4>
<hr/>
@if(getUserInfo($loginUser->id)->email_verified)
    <p><span class="text text-success"><i class="fa fa-check"></i> @lang('crypto.your_email_was_verified')</span></p>
@else
    <p>@lang('crypto.email_not_verified')</p>
    <form action="{{route('account.doVerification')}}" method="POST">
        {{csrf_field()}}
        <input type="hidden" name="verify" value="email">
        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> @lang('crypto.btn_send_verification_email')</button>
    </form>
@endif
<br>