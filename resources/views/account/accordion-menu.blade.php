@php
    $wallet = '';
    $transaction = '';
    $advertisements = '';
    $trades = '';
    $settings = '';
    $verification = '';
    if(Request::is('account/wallet')){
        $wallet = 'active';
    }elseif(Request::is('account/transactions')){
        $transaction = 'active';
    }elseif(Request::is('account/advertisements/*')){
        $advertisements = 'active';
    }elseif(Request::is('account/trades')){
        $trades = 'active';
    }elseif(Request::is('account/settings')){
        $settings = 'active';
    }elseif(Request::is('account/verification')){
        $verification = 'active';
    }
@endphp

<div class="list-group">
    <a href="{{route('account.wallet')}}" class="list-group-item {{$wallet}}"><i class="fa fa-bitcoin"></i> @lang('crypto.menu_wallet')</a>
    <a href="{{route('account.transaction')}}" class="list-group-item {{$transaction}}"><i class="fa fa-exchange"></i> @lang('crypto.menu_transactions')</a>
    <a href="{{route('account.advertisement')}}" class="list-group-item {{$advertisements}}"><i class="fa fa-globe"></i> @lang('crypto.menu_advertisements')</a>
    <a href="{{route('account.trade')}}" class="list-group-item {{$trades}}"><i class="fa fa-refresh"></i> @lang('crypto.menu_trades')</a>
    <a href="{{route('account.setting')}}" class="list-group-item {{$settings}}"><i class="fa fa-cogs"></i> @lang('crypto.menu_settings')</a>

    <a href="{{route('account.verification')}}" class="list-group-item {{$verification}}"><i class="fa fa-check"></i> @lang('crypto.menu_verification')</a>
</div>