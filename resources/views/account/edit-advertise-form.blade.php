<form action="{{route('account.advertisementUpdate',$model->id)}}" method="POST">
    {{csrf_field()}}
    <div class="col-md-12">
        @lang('crypto.current_ad_type'): {{$type}}
    <br><br>
    </div>
    <input type="hidden" id="ad_type2" value="{{$model->type}}">
    <div class="col-md-6">
        <div class="form-group">
            <label>@lang('crypto.payment_method')</label>
            <select class="form-control" name="payment_method">
                <option value="PayPal" <?php if($model->payment_method == "PayPal") { echo 'selected'; } ?>>PayPal</option>
                <option value="Skrill" <?php if($model->payment_method == "Skrill") { echo 'selected'; } ?>>Skrill</option>
                <option value="Payeer" <?php if($model->payment_method == "Payeer") { echo 'selected'; } ?>>Payeer</option>
                <option value="Xoomwallet" <?php if($model->payment_method == "Xoomwallet") { echo 'selected'; } ?>>Xoomwallet</option>
                <option value="Perfect Money" <?php if($model->payment_method == "Perfect Money") { echo 'selected'; } ?>>Perfect Money</option>
                <option value="Payoneer" <?php if($model->payment_method == "Payoneer") { echo 'selected'; } ?>>Payoneer</option>
                <option value="AdvCash" <?php if($model->payment_method == "AdvCash") { echo 'selected'; } ?>>AdvCash</option>
                <option value="OKPay" <?php if($model->payment_method == "OKPay") { echo 'selected'; } ?>>OKPay</option>
                <option value="Entromoney" <?php if($model->payment_method == "Entromoney") { echo 'selected'; } ?>>Entromoney</option>
                <option value="SolidTrust Pay" <?php if($model->payment_method == "SolidTrust Pay") { echo 'selected'; } ?>>SolidTrust Pay</option>
                <option value="Neteller" <?php if($model->payment_method == "Neteller") { echo 'selected'; } ?>>Neteller</option>
                <option value="UQUID" <?php if($model->payment_method == "UQUID") { echo 'selected'; } ?>>UQUID</option>
                <option value="Yandex Money" <?php if($model->payment_method == "Yandex Money") { echo 'selected'; } ?>>Yandex Money</option>
                <option value="QIWI" <?php if($model->payment_method == "QIWI") { echo 'selected'; } ?>>QIWI</option>
                <option value="Bank Transfer" <?php if($model->payment_method == "Bank Transfer") { echo 'selected'; } ?>>Bank Transfer</option>
                <option value="Western Union" <?php if($model->payment_method == "Western Union") { echo 'selected'; } ?>>Western Union</option>
                <option value="Moneygram" <?php if($model->payment_method == "Moneygram") { echo 'selected'; } ?>>Moneygram</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <br><span class="text text-muted">@lang('crypto.payment_method_info')</span>
    </div>
    <div class="col-md-12"><br></div>
    <div class="col-md-6">
        <div class="form-group">
            <label>@lang('crypto.currency')</label>
            <select class="form-control" name="currency" onchange="btc_calculate_price();" id="btc_currency">
                    <option value="AED" <?php if($model->currency == "AED") { echo 'selected'; } ?>>AED - United Arab Emirates Dirham</option>
                    <option value="AFN" <?php if($model->currency == "AFN") { echo 'selected'; } ?>>AFN - Afghanistan Afghani</option>
                    <option value="ALL" <?php if($model->currency == "ALL") { echo 'selected'; } ?>>ALL - Albania Lek</option>
                    <option value="AMD" <?php if($model->currency == "AMD") { echo 'selected'; } ?>>AMD - Armenia Dram</option>
                    <option value="ANG" <?php if($model->currency == "ANG") { echo 'selected'; } ?>>ANG - Netherlands Antilles Guilder</option>
                    <option value="AOA" <?php if($model->currency == "AOA") { echo 'selected'; } ?>>AOA - Angola Kwanza</option>
                    <option value="ARS" <?php if($model->currency == "ARS") { echo 'selected'; } ?>>ARS - Argentina Peso</option>
                    <option value="AUD" <?php if($model->currency == "AUD") { echo 'selected'; } ?>>AUD - Australia Dollar</option>
                    <option value="AWG" <?php if($model->currency == "AWG") { echo 'selected'; } ?>>AWG - Aruba Guilder</option>
                    <option value="AZN" <?php if($model->currency == "AZN") { echo 'selected'; } ?>>AZN - Azerbaijan New Manat</option>
                    <option value="BAM" <?php if($model->currency == "BAM") { echo 'selected'; } ?>>BAM - Bosnia and Herzegovina Convertible Marka</option>
                    <option value="BBD" <?php if($model->currency == "BBD") { echo 'selected'; } ?>>BBD - Barbados Dollar</option>
                    <option value="BDT" <?php if($model->currency == "BDT") { echo 'selected'; } ?>>BDT - Bangladesh Taka</option>
                    <option value="BGN" <?php if($model->currency == "BGN") { echo 'selected'; } ?>>BGN - Bulgaria Lev</option>
                    <option value="BHD" <?php if($model->currency == "BHD") { echo 'selected'; } ?>>BHD - Bahrain Dinar</option>
                    <option value="BIF" <?php if($model->currency == "BIF") { echo 'selected'; } ?>>BIF - Burundi Franc</option>
                    <option value="BMD" <?php if($model->currency == "BMD") { echo 'selected'; } ?>>BMD - Bermuda Dollar</option>
                    <option value="BND" <?php if($model->currency == "BND") { echo 'selected'; } ?>>BND - Brunei Darussalam Dollar</option>
                    <option value="BOB" <?php if($model->currency == "BOB") { echo 'selected'; } ?>>BOB - Bolivia Boliviano</option>
                    <option value="BRL" <?php if($model->currency == "BRL") { echo 'selected'; } ?>>BRL - Brazil Real</option>
                    <option value="BSD" <?php if($model->currency == "BSD") { echo 'selected'; } ?>>BSD - Bahamas Dollar</option>
                    <option value="BTN" <?php if($model->currency == "BTN") { echo 'selected'; } ?>>BTN - Bhutan Ngultrum</option>
                    <option value="BWP" <?php if($model->currency == "BWP") { echo 'selected'; } ?>>BWP - Botswana Pula</option>
                    <option value="BYR" <?php if($model->currency == "BYR") { echo 'selected'; } ?>>BYR - Belarus Ruble</option>
                    <option value="BZD" <?php if($model->currency == "BZD") { echo 'selected'; } ?>>BZD - Belize Dollar</option>
                    <option value="CAD" <?php if($model->currency == "CAD") { echo 'selected'; } ?>>CAD - Canada Dollar</option>
                    <option value="CDF" <?php if($model->currency == "CDF") { echo 'selected'; } ?>>CDF - Congo/Kinshasa Franc</option>
                    <option value="CHF" <?php if($model->currency == "CHF") { echo 'selected'; } ?>>CHF - Switzerland Franc</option>
                    <option value="CLP" <?php if($model->currency == "CLP") { echo 'selected'; } ?>>CLP - Chile Peso</option>
                    <option value="CNY" <?php if($model->currency == "CNY") { echo 'selected'; } ?>>CNY - China Yuan Renminbi</option>
                    <option value="COP" <?php if($model->currency == "COP") { echo 'selected'; } ?>>COP - Colombia Peso</option>
                    <option value="CRC" <?php if($model->currency == "CRC") { echo 'selected'; } ?>>CRC - Costa Rica Colon</option>
                    <option value="CUC" <?php if($model->currency == "CUC") { echo 'selected'; } ?>>CUC - Cuba Convertible Peso</option>
                    <option value="CUP" <?php if($model->currency == "CUP") { echo 'selected'; } ?>>CUP - Cuba Peso</option>
                    <option value="CVE" <?php if($model->currency == "CVE") { echo 'selected'; } ?>>CVE - Cape Verde Escudo</option>
                    <option value="CZK" <?php if($model->currency == "CZK") { echo 'selected'; } ?>>CZK - Czech Republic Koruna</option>
                    <option value="DJF" <?php if($model->currency == "DJF") { echo 'selected'; } ?>>DJF - Djibouti Franc</option>
                    <option value="DKK" <?php if($model->currency == "DKK") { echo 'selected'; } ?>>DKK - Denmark Krone</option>
                    <option value="DOP" <?php if($model->currency == "DOP") { echo 'selected'; } ?>>DOP - Dominican Republic Peso</option>
                    <option value="DZD" <?php if($model->currency == "DZD") { echo 'selected'; } ?>>DZD - Algeria Dinar</option>
                    <option value="EGP" <?php if($model->currency == "EGP") { echo 'selected'; } ?>>EGP - Egypt Pound</option>
                    <option value="ERN" <?php if($model->currency == "ERN") { echo 'selected'; } ?>>ERN - Eritrea Nakfa</option>
                    <option value="ETB" <?php if($model->currency == "ETB") { echo 'selected'; } ?>>ETB - Ethiopia Birr</option>
                    <option value="EUR" <?php if($model->currency == "EUR") { echo 'selected'; } ?>>EUR - Euro Member Countries</option>
                    <option value="FJD" <?php if($model->currency == "FJD") { echo 'selected'; } ?>>FJD - Fiji Dollar</option>
                    <option value="FKP" <?php if($model->currency == "FKP") { echo 'selected'; } ?>>FKP - Falkland Islands (Malvinas) Pound</option>
                    <option value="GBP" <?php if($model->currency == "GBP") { echo 'selected'; } ?>>GBP - United Kingdom Pound</option>
                    <option value="GEL" <?php if($model->currency == "GEL") { echo 'selected'; } ?>>GEL - Georgia Lari</option>
                    <option value="GGP" <?php if($model->currency == "GGP") { echo 'selected'; } ?>>GGP - Guernsey Pound</option>
                    <option value="GHS" <?php if($model->currency == "GHS") { echo 'selected'; } ?>>GHS - Ghana Cedi</option>
                    <option value="GIP" <?php if($model->currency == "GIP") { echo 'selected'; } ?>>GIP - Gibraltar Pound</option>
                    <option value="GMD" <?php if($model->currency == "GMD") { echo 'selected'; } ?>>GMD - Gambia Dalasi</option>
                    <option value="GNF" <?php if($model->currency == "GNF") { echo 'selected'; } ?>>GNF - Guinea Franc</option>
                    <option value="GTQ" <?php if($model->currency == "GTQ") { echo 'selected'; } ?>>GTQ - Guatemala Quetzal</option>
                    <option value="GYD" <?php if($model->currency == "GYD") { echo 'selected'; } ?>>GYD - Guyana Dollar</option>
                    <option value="HKD" <?php if($model->currency == "HKD") { echo 'selected'; } ?>>HKD - Hong Kong Dollar</option>
                    <option value="HNL" <?php if($model->currency == "HNL") { echo 'selected'; } ?>>HNL - Honduras Lempira</option>
                    <option value="HPK" <?php if($model->currency == "HPK") { echo 'selected'; } ?>>HRK - Croatia Kuna</option>
                    <option value="HTG" <?php if($model->currency == "HTG") { echo 'selected'; } ?>>HTG - Haiti Gourde</option>
                    <option value="HUF" <?php if($model->currency == "HUF") { echo 'selected'; } ?>>HUF - Hungary Forint</option>
                    <option value="IDR" <?php if($model->currency == "IDR") { echo 'selected'; } ?>>IDR - Indonesia Rupiah</option>
                    <option value="ILS" <?php if($model->currency == "TLS") { echo 'selected'; } ?>>ILS - Israel Shekel</option>
                    <option value="IMP" <?php if($model->currency == "IMP") { echo 'selected'; } ?>>IMP - Isle of Man Pound</option>
                    <option value="INR" <?php if($model->currency == "INR") { echo 'selected'; } ?>>INR - India Rupee</option>
                    <option value="IQD" <?php if($model->currency == "IDQ") { echo 'selected'; } ?>>IQD - Iraq Dinar</option>
                    <option value="IRR" <?php if($model->currency == "IRR") { echo 'selected'; } ?>>IRR - Iran Rial</option>
                    <option value="ISK" <?php if($model->currency == "ISK") { echo 'selected'; } ?>>ISK - Iceland Krona</option>
                    <option value="JEP" <?php if($model->currency == "JEP") { echo 'selected'; } ?>>JEP - Jersey Pound</option>
                    <option value="JMD" <?php if($model->currency == "JMD") { echo 'selected'; } ?>>JMD - Jamaica Dollar</option>
                    <option value="JOD" <?php if($model->currency == "JOD") { echo 'selected'; } ?>>JOD - Jordan Dinar</option>
                    <option value="JPY" <?php if($model->currency == "JPY") { echo 'selected'; } ?>>JPY - Japan Yen</option>
                    <option value="KES" <?php if($model->currency == "KES") { echo 'selected'; } ?>>KES - Kenya Shilling</option>
                    <option value="KGS" <?php if($model->currency == "KGS") { echo 'selected'; } ?>>KGS - Kyrgyzstan Som</option>
                    <option value="KHR" <?php if($model->currency == "KHR") { echo 'selected'; } ?>>KHR - Cambodia Riel</option>
                    <option value="KMF" <?php if($model->currency == "KMF") { echo 'selected'; } ?>>KMF - Comoros Franc</option>
                    <option value="KPW" <?php if($model->currency == "KPW") { echo 'selected'; } ?>>KPW - Korea (North) Won</option>
                    <option value="KRW" <?php if($model->currency == "KRW") { echo 'selected'; } ?>>KRW - Korea (South) Won</option>
                    <option value="KWD" <?php if($model->currency == "KWD") { echo 'selected'; } ?>>KWD - Kuwait Dinar</option>
                    <option value="KYD" <?php if($model->currency == "KYD") { echo 'selected'; } ?>>KYD - Cayman Islands Dollar</option>
                    <option value="KZT" <?php if($model->currency == "KZT") { echo 'selected'; } ?>>KZT - Kazakhstan Tenge</option>
                    <option value="LAK" <?php if($model->currency == "LAK") { echo 'selected'; } ?>>LAK - Laos Kip</option>
                    <option value="LBP" <?php if($model->currency == "LBP") { echo 'selected'; } ?>>LBP - Lebanon Pound</option>
                    <option value="LKR" <?php if($model->currency == "LKR") { echo 'selected'; } ?>>LKR - Sri Lanka Rupee</option>
                    <option value="LRD" <?php if($model->currency == "LRD") { echo 'selected'; } ?>>LRD - Liberia Dollar</option>
                    <option value="LSL" <?php if($model->currency == "LSL") { echo 'selected'; } ?>>LSL - Lesotho Loti</option>
                    <option value="LYD" <?php if($model->currency == "LYD") { echo 'selected'; } ?>>LYD - Libya Dinar</option>
                    <option value="MAD" <?php if($model->currency == "MAD") { echo 'selected'; } ?>>MAD - Morocco Dirham</option>
                    <option value="MDL" <?php if($model->currency == "MDL") { echo 'selected'; } ?>>MDL - Moldova Leu</option>
                    <option value="MGA" <?php if($model->currency == "MGA") { echo 'selected'; } ?>>MGA - Madagascar Ariary</option>
                    <option value="MKD" <?php if($model->currency == "MKD") { echo 'selected'; } ?>>MKD - Macedonia Denar</option>
                    <option value="MMK" <?php if($model->currency == "MMK") { echo 'selected'; } ?>>MMK - Myanmar (Burma) Kyat</option>
                    <option value="MNT" <?php if($model->currency == "MNT") { echo 'selected'; } ?>>MNT - Mongolia Tughrik</option>
                    <option value="MOP" <?php if($model->currency == "MOP") { echo 'selected'; } ?>>MOP - Macau Pataca</option>
                    <option value="MRO" <?php if($model->currency == "MRO") { echo 'selected'; } ?>>MRO - Mauritania Ouguiya</option>
                    <option value="MUR" <?php if($model->currency == "MUR") { echo 'selected'; } ?>>MUR - Mauritius Rupee</option>
                    <option value="MVR" <?php if($model->currency == "MVR") { echo 'selected'; } ?>>MVR - Maldives (Maldive Islands) Rufiyaa</option>
                    <option value="MWK" <?php if($model->currency == "MWK") { echo 'selected'; } ?>>MWK - Malawi Kwacha</option>
                    <option value="MXN" <?php if($model->currency == "MXN") { echo 'selected'; } ?>>MXN - Mexico Peso</option>
                    <option value="MYR" <?php if($model->currency == "MYR") { echo 'selected'; } ?>>MYR - Malaysia Ringgit</option>
                    <option value="MZN" <?php if($model->currency == "MZN") { echo 'selected'; } ?>>MZN - Mozambique Metical</option>
                    <option value="NAD" <?php if($model->currency == "NAD") { echo 'selected'; } ?>>NAD - Namibia Dollar</option>
                    <option value="NGN" <?php if($model->currency == "NGN") { echo 'selected'; } ?>>NGN - Nigeria Naira</option>
                    <option value="NTO" <?php if($model->currency == "NTO") { echo 'selected'; } ?>>NIO - Nicaragua Cordoba</option>
                    <option value="NOK" <?php if($model->currency == "NOK") { echo 'selected'; } ?>>NOK - Norway Krone</option>
                    <option value="NPR" <?php if($model->currency == "NPR") { echo 'selected'; } ?>>NPR - Nepal Rupee</option>
                    <option value="NZD" <?php if($model->currency == "NZD") { echo 'selected'; } ?>>NZD - New Zealand Dollar</option>
                    <option value="OMR" <?php if($model->currency == "OMR") { echo 'selected'; } ?>>OMR - Oman Rial</option>
                    <option value="PAB" <?php if($model->currency == "PAB") { echo 'selected'; } ?>>PAB - Panama Balboa</option>
                    <option value="PEN" <?php if($model->currency == "PEN") { echo 'selected'; } ?>>PEN - Peru Nuevo Sol</option>
                    <option value="PGK" <?php if($model->currency == "PHK") { echo 'selected'; } ?>>PGK - Papua New Guinea Kina</option>
                    <option value="PHP" <?php if($model->currency == "PHP") { echo 'selected'; } ?>>PHP - Philippines Peso</option>
                    <option value="PKR" <?php if($model->currency == "PKR") { echo 'selected'; } ?>>PKR - Pakistan Rupee</option>
                    <option value="PLN" <?php if($model->currency == "PLN") { echo 'selected'; } ?>>PLN - Poland Zloty</option>
                    <option value="PYG" <?php if($model->currency == "PYG") { echo 'selected'; } ?>>PYG - Paraguay Guarani</option>
                    <option value="QAR" <?php if($model->currency == "QAR") { echo 'selected'; } ?>>QAR - Qatar Riyal</option>
                    <option value="RON" <?php if($model->currency == "RON") { echo 'selected'; } ?>>RON - Romania New Leu</option>
                    <option value="RSD" <?php if($model->currency == "RSD") { echo 'selected'; } ?>>RSD - Serbia Dinar</option>
                    <option value="RUB" <?php if($model->currency == "RUB") { echo 'selected'; } ?>>RUB - Russia Ruble</option>
                    <option value="RWF" <?php if($model->currency == "RWF") { echo 'selected'; } ?>>RWF - Rwanda Franc</option>
                    <option value="SAR" <?php if($model->currency == "SAR") { echo 'selected'; } ?>>SAR - Saudi Arabia Riyal</option>
                    <option value="SBD" <?php if($model->currency == "SBD") { echo 'selected'; } ?>>SBD - Solomon Islands Dollar</option>
                    <option value="SCR" <?php if($model->currency == "SCR") { echo 'selected'; } ?>>SCR - Seychelles Rupee</option>
                    <option value="SDG" <?php if($model->currency == "SDG") { echo 'selected'; } ?>>SDG - Sudan Pound</option>
                    <option value="SEK" <?php if($model->currency == "SEK") { echo 'selected'; } ?>>SEK - Sweden Krona</option>
                    <option value="SGD" <?php if($model->currency == "SGD") { echo 'selected'; } ?>>SGD - Singapore Dollar</option>
                    <option value="SHP" <?php if($model->currency == "SHP") { echo 'selected'; } ?>>SHP - Saint Helena Pound</option>
                    <option value="SLL" <?php if($model->currency == "SLL") { echo 'selected'; } ?>>SLL - Sierra Leone Leone</option>
                    <option value="SOS" <?php if($model->currency == "SOS") { echo 'selected'; } ?>>SOS - Somalia Shilling</option>
                    <option value="SRL" <?php if($model->currency == "SRL") { echo 'selected'; } ?>>SPL* - Seborga Luigino</option>
                    <option value="SRD" <?php if($model->currency == "SRD") { echo 'selected'; } ?>>SRD - Suriname Dollar</option>
                    <option value="STD" <?php if($model->currency == "STD") { echo 'selected'; } ?>>STD - Sao Tome and Principe Dobra</option>
                    <option value="SVC" <?php if($model->currency == "SVC") { echo 'selected'; } ?>>SVC - El Salvador Colon</option>
                    <option value="SYP" <?php if($model->currency == "SYP") { echo 'selected'; } ?>>SYP - Syria Pound</option>
                    <option value="SZL" <?php if($model->currency == "SZL") { echo 'selected'; } ?>>SZL - Swaziland Lilangeni</option>
                    <option value="THB" <?php if($model->currency == "THB") { echo 'selected'; } ?>>THB - Thailand Baht</option>
                    <option value="TJS" <?php if($model->currency == "TJS") { echo 'selected'; } ?>>TJS - Tajikistan Somoni</option>
                    <option value="TMT" <?php if($model->currency == "TMT") { echo 'selected'; } ?>>TMT - Turkmenistan Manat</option>
                    <option value="TND" <?php if($model->currency == "TND") { echo 'selected'; } ?>>TND - Tunisia Dinar</option>
                    <option value="TOP" <?php if($model->currency == "TOP") { echo 'selected'; } ?>>TOP - Tonga Pa'anga</option>
                    <option value="TRY" <?php if($model->currency == "TRY") { echo 'selected'; } ?>>TRY - Turkey Lira</option>
                    <option value="TTD" <?php if($model->currency == "TTD") { echo 'selected'; } ?>>TTD - Trinidad and Tobago Dollar</option>
                    <option value="TVD" <?php if($model->currency == "TVD") { echo 'selected'; } ?>>TVD - Tuvalu Dollar</option>
                    <option value="TWD" <?php if($model->currency == "TWD") { echo 'selected'; } ?>>TWD - Taiwan New Dollar</option>
                    <option value="TZS" <?php if($model->currency == "TZS") { echo 'selected'; } ?>>TZS - Tanzania Shilling</option>
                    <option value="UAH" <?php if($model->currency == "UAH") { echo 'selected'; } ?>>UAH - Ukraine Hryvnia</option>
                    <option value="UGX" <?php if($model->currency == "UGX") { echo 'selected'; } ?>>UGX - Uganda Shilling</option>
                    <option value="USD"  <?php if($model->currency == "USD") { echo 'selected'; } ?>>USD - United States Dollar</option>
                    <option value="UYU" <?php if($model->currency == "UYU") { echo 'selected'; } ?>>UYU - Uruguay Peso</option>
                    <option value="UZS" <?php if($model->currency == "UZS") { echo 'selected'; } ?>>UZS - Uzbekistan Som</option>
                    <option value="VEF" <?php if($model->currency == "VEF") { echo 'selected'; } ?>>VEF - Venezuela Bolivar</option>
                    <option value="VND" <?php if($model->currency == "VND") { echo 'selected'; } ?>>VND - Viet Nam Dong</option>
                    <option value="VUV" <?php if($model->currency == "VUV") { echo 'selected'; } ?>>VUV - Vanuatu Vatu</option>
                    <option value="WST" <?php if($model->currency == "WST") { echo 'selected'; } ?>>WST - Samoa Tala</option>
                    <option value="XAF" <?php if($model->currency == "XAF") { echo 'selected'; } ?>>XAF - Communaute Financiere Africaine (BEAC) CFA Franc BEAC</option>
                    <option value="XCD" <?php if($model->currency == "XCD") { echo 'selected'; } ?>>XCD - East Caribbean Dollar</option>
                    <option value="XDR" <?php if($model->currency == "XDR") { echo 'selected'; } ?>>XDR - International Monetary Fund (IMF) Special Drawing Rights</option>
                    <option value="XOF" <?php if($model->currency == "XOF") { echo 'selected'; } ?>>XOF - Communaute Financiere Africaine (BCEAO) Franc</option>
                    <option value="XPF" <?php if($model->currency == "XPF") { echo 'selected'; } ?>>XPF - Comptoirs Francais du Pacifique (CFP) Franc</option>
                    <option value="YER" <?php if($model->currency == "YER") { echo 'selected'; } ?>>YER - Yemen Rial</option>
                    <option value="ZAR" <?php if($model->currency == "ZAR") { echo 'selected'; } ?>>ZAR - South Africa Rand</option>
                    <option value="ZMW" <?php if($model->currency == "ZMW") { echo 'selected'; } ?>>ZMW - Zambia Kwacha</option>
                    <option value="ZWD" <?php if($model->currency == "ZWD") { echo 'selected'; } ?>>ZWD - Zimbabwe Dollar</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <br><span class="text text-muted">@lang('crypto.currency_info')</span>
    </div>
    <div class="col-md-12"><br></div>
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('crypto.enter_comission')</label>
                <input type="text" class="form-control" name="amount" value="{{$model->price}}" onkeyup="btc_calculate_price();" onkeydown="btc_calculate_price();" id="btc_amount">
                <input type="hidden" name="type" value="{{$model->type}}">
                @lang('crypto.currenct_bitcoin_price'): {{$current_price}} USD<br/>
                <span id="your_btc_price">@lang('crypto.your_bitcoin_price'): {{$crypto_price}} {{$model->currency}}</span>
            </div>
        </div>
        <div class="col-md-6">
            <br><span class="text text-muted">@lang('crypto.enter_comission_info')</span>
        </div>
    <div class="col-md-12"><br></div>
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('crypto.payment_instructions')</label>
                <textarea class="form-control" name="payment_instructions" rows="5">{{$model->payment_instructions}}</textarea>
            </div>
        </div>
        <div class="col-md-6">
            <br><span class="text text-muted">@lang('crypto.payment_instructions_info')</span>
        </div>
        <div class="col-md-12"><br></div>
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('crypto.minimal_trade_amount')</label>
                <input type="text" class="form-control" name="min_amount" value="{{$model->min_amount}}">
            </div>
        </div>
        <div class="col-md-6">
            <br><span class="text text-muted">@lang('crypto.minimal_trade_amount_info')</span>
        </div>
        <div class="col-md-12"><br></div>
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('crypto.maximum_trade_amount')</label>
                <input type="text" class="form-control" name="max_amount" value="{{$model->max_amount}}">
            </div>
        </div>
        <div class="col-md-6">
            <br><span class="text text-muted">@lang('crypto.maximum_trade_amount_info')</span>
        </div>
        <div class="col-md-12"><br></div>
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('crypto.trade_process_time')</label>
                <input type="text" class="form-control" name="process_time" value="{{$model->process_time}}">
            </div>
        </div>
        <div class="col-md-6">
            <br><span class="text text-muted">@lang('crypto.trade_process_time_info')</span>
        </div>
        <div class="col-md-12"><br></div>
        <div class="col-md-6">
            <div class="form-group">
                <label>@lang('crypto.terms_of_trade')</label>
                <textarea class="form-control" name="terms" rows="5">{{$model->terms}}</textarea>
            </div>
        </div>
        <div class="col-md-6">
            <br><span class="text text-muted">@lang('crypto.terms_of_trade_info')</span>
        </div>
        <?php if(getSettingsData()->document_verification) { ?>
        <div class="col-md-12">
             <div class="checkbox">
                <label>
                  <input type="checkbox" name="require_document" value="yes" <?php if($model->require_document) { echo 'checked'; } ?>> @lang('crypto.require_document')
                </label>
              </div>
        </div>
        <?php } ?>
        <?php if(getSettingsData()->email_verification) { ?>
        <div class="col-md-12">
             <div class="checkbox">
                <label>
                  <input type="checkbox" name="require_email" value="yes" <?php if($model->require_email) { echo 'checked'; } ?>> @lang('crypto.require_email')
                </label>
              </div>
        </div>
        <?php } ?>
        <?php if(getSettingsData()->phone_verification) { ?>
        <div class="col-md-12">
             <div class="checkbox">
                <label>
                  <input type="checkbox" name="require_mobile" value="yes" <?php if($model->require_mobile) { echo 'checked'; } ?>> @lang('crypto.require_mobile')
                </label>
              </div>
        </div>
        <?php } ?>
        <div class="col-md-12">
        <button type="submit" class="btn btn-primary" name="btc_save"><i class="fa fa-check"></i> @lang('crypto.btn_save_changes')</button>
        </div>
</form>