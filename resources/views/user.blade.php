@extends('layouts.master')
@section('content')

	<div class="container-fluid page-title">
			<div class="row blue-banner">
            	<div class="container main-container">
                	<div class="col-lg-12 col-md-12 col-sm-12">
							<h2 class="white-heading"><i class="fa fa-user"></i> <b>{{$model->username}}</b> <small style="color:#fff;">- </small></h2>
							<h4 style="color:#fff;"><i class="fa fa-globe"></i> {{$totalAdd}} @lang('crypto.advertisements'), <i class="fa fa-refresh"></i> {{$totalTrade}} @lang('crypto.trades')</h4>
					</div>
                </div>
            </div>
        </div>
  	 <!--header section -->
	 <!-- full width section -->
    	<div class="container-fluid white-bg">
        	<div class="row">
            	<div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
							<div class="row">
								<div class="col-md-8">
									<div class="panel panel-default">
										<div class="panel-body">
											<h3>@lang('crypto.aadvertisements')</h3>
											<table class="table table-striped">
												<thead>
													<tr>
														<th width="30%">@lang('crypto.ad_type')</th>
														<th width="25%">@lang('crypto.payment_method')</th>
														<th width="20%">@lang('crypto.price')</th>
														<th width="20%">@lang('crypto.limits')</th>
														<th width="5%"></th>
													</tr>
												</thead>
												<tbody style="font-size:14px;">

														<tr><td colspan="5">@lang('crypto.no_ad_for_display')</td></tr>

												</tbody>
											</table>
										</div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="panel panel-default">
										<div class="panel-body">
											<h3>@lang('crypto.latest_feedbacks')</h3>
											@if(count($rating)>0)
												@foreach($rating as $single)
													@php
														$adid = tradeinfo($single->trade_id,"ad_id");
														$adpaymentmethod = adinfo($adid,"payment_method");
														$adtype = adinfo($adid,"type");
														if($adtype == "buy") {
															$pm = str_ireplace(" ","-",$adpaymentmethod);
															$adlink = $settings['url']."ad/Bitcoin-to-".$pm."/".$adid;
														} elseif($adtype == "sell") {
															$pm = str_ireplace(" ","-",$adpaymentmethod);
															$adlink = $settings['url']."ad/".$pm."-to-Bitcoin/".$adid;
														} else { }

													@endphp
													<div class="row">
														<div class="col-md-2">
															@if($single->type == "1")
															<span class="text text-success"><i class="fa fa-smile-o fa-3x"></i></span>
															@elseif($single->type == "2")
															<span class="text text-warning"><i class="fa fa-meh-o fa-3x"></i></span>
															@elseif($single->type == "3")
															<span class="text text-danger"><i class="fa fa-frown-o fa-3x"></i></span>
															@endif
														</div>
														<div class="col-md-10">
															<a href="{{route('userProfile',getUserInfo($single->uid)->username)}}">{{getUserInfo($single->uid)->username}}</a> <i class="fa fa-angle-right"></i> <a href="{{route('userProfile',getUserInfo($single->uid)->username)}}">{{getUserInfo($single->uid)->username}}</a><br/>
															<p>{{$single->comment}}</p>
															@lang('crypto.advertisements') <a href="{{route('postAdView',$adid)}}">#{{$adid}}</a>
														</div>
													</div>
													<hr/>
												@endforeach
											@else
												@lang('crypto.no_feedbacks_yet');
											@endif
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>>


@endsection