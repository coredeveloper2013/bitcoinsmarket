<!--Footer Area-->
   		<div class="container-fluid footer">
        	<div class="row">
            <div class="container main-container-home">
            	<div class="col-md-3">
					<h3>@lang('crypto.pages')</h3>
					 <ul class="list-unstyled">
						<li><a href="{{route('faq')}}">@lang('crypto.faq')</a></li>
						<li><a href="{{route('termsOfService')}}">@lang('crypto.terms_of_services')</a></li>
						<li><a href="{{route('privacy')}}">@lang('crypto.privacy_policy')</a></li>

					</ul>
				</div>
				<div class="col-md-4">
					<h3>languages</h3>
					 <ul class="list-unstyled">
						<?php //echo getLanguage($settings['url'],null,1); ?>
						 <li><a href="">English</a></li>
					</ul>
				</div>
				<div class="col-md-5">
					<h3>@lang('crypto.follow_us')</h3>
					Twitter or Facebook code here.
				</div>
			</div>

            </div>
            </div>
        </div>
    <!--Footer Area-->
    <!--Last Footer Area---->
    	<div class="container-fluid footer last-footer ">
        	<div class="row">
            <div class="container main-container">
            	<div class="col-lg-9 col-md-3 col-sm-9 col-xs-6" >
                	<p class="copyright">Copyright © 2017. Developed by <a href="http://www.exchangesoftware.info">www.exchangesoftware.info</a></p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                	<ul class="list-group">
                    	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>

            </div>
            </div>
        </div>
    <!--Last Footer Area---->
