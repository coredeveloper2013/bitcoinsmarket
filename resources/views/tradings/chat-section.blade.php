<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-body">
            <script type="text/javascript">

                $(function(){
                    var btn = document.getElementById('uploadBtn');
                    var trade_id = '{{$model->id}}';
                    var url = window.location.href.toString()+'/../../../../';
                    var url = $("#url").val();
                    var uploader = new ss.SimpleUpload({
                        button: btn,
                        url: url+'/upload-from-chat?trade_id={{$model->id}}',
                        name: 'uploadFile',
                        onComplete: function( filename, response ) {
                            btc_check_new_file_messages('{{$loginUser->id}}','{{$model->id}}');
                          },
                           onError: function() {
                            btc_check_new_file_messages('{{$loginUser->id}}','{{$model->id}}');
                          }
                    });
                });

                function btc_int_cnm() {
                    btc_check_new_messages('{{$loginUser->id}}','{{$model->id}}');
                    btc_check_trade_status('{{$model->id}}');
                }

                setInterval(btc_int_cnm,3000);
            </script>
            <h3>@lang('crypto.chat')</h3>
            <form id="trade_chat_form_{{$model->id}}">
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="3" placeholder="@lang('crypto.write_message_to_trader')"></textarea>
                </div>
                <button type="button" class="btn btn-warning" onclick="btc_post_trade_message('{{$model->id}}');">@lang('crypto.btn_send_message')</button>
                <button type="button" class="btn btn-success" id="uploadBtn">Upload file</button>
            </form>
            <hr/>
            <div id="trade_chat_{{$model->id}}">
                @php
                    $tradeMsg = getTradesMsg($model->id);
                @endphp

                @if(count($tradeMsg)>0)
                    @foreach($tradeMsg as $singleMsg)
                        @if($loginUser->id == $singleMsg->uid)
                            @php readTradeMsg($singleMsg->id); @endphp
                        @endif

                        @if($singleMsg->attachment)
                            @php $filename = basename($singleMsg->message); @endphp
                            <div style="font-size:14px;">
                                <b><a href="{{route('userProfile',getUserInfo($model->uid)->username)}}">{{getUserInfo($model->uid)->username}} </a></b> attach file<br/>
                                <i class="fa fa-file-o"></i> <a href="{{url('/'.$singleMsg->message)}}" target="_blank">{{$filename}}</a><br/>
                                <span class="text text-muted" style="font-size:11px;">{{timeago($singleMsg->time)}}</span>
                            </div>
                            <hr/>
                        @else
                            <div style="font-size:14px;">
                                <b><a href="">{{getUserInfo($model->uid)->username}}</a></b>: {{$singleMsg->message}}<br/>
                                <span class="text text-muted" style="font-size:11px;">{{timeago($singleMsg->time)}}</span>
                            </div>
                            <hr/>
                        @endif

                    @endforeach
                @else
                    @lang('crypto.no_have_messages')
                @endif

            </div>
        </div>
    </div>
</div>