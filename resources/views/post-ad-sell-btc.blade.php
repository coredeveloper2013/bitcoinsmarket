<div class="container-fluid page-title">
    <div class="row blue-banner">
        <div class="container main-container">
            <div class="row">
                <div class="col-md-12">
                    <span style="color:#fff;font-size:35px;font-weight:bold;">@lang('crypto.advertisement') #{{$model->id}}</span><br/>
                    <span style="color:#fff;font-size:25px;">@lang('crypto.buy_bitcoins_via') {{$model->payment_method}} @lang('crypto.for') {{convertBTCprice($model->price,$model->currency)}} {{$model->currency}}/BTC</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--header section -->

<div class="container-fluid white-bg">
    <div class="row">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            @include('flash-message')
                        </div>
                        <div class="col-md-8">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h3>@lang('crypto.trader') <a href="asdf" id="user_status" data-toggle="tooltip" data-placement="top" title="{{activity_time($model->uid)}}">{{$model->User->username}}</a></h3>
                                    <p>

                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><span class="text text-success"><i class="fa fa-smile-o"></i> {{getFeedback($model->uid,1)}} @lang('crypto.positive_feedbacks')</span></td>
                                                <td><span class="text text-warning"><i class="fa fa-meh-o"></i> {{getFeedback($model->uid,2)}} @lang('crypto.neutral_feedbacks')</span></td>
                                                <td><span class="text text-danger"><i class="fa fa-frown-o"></i> {{getFeedback($model->uid,3)}} @lang('crypto.negative_feedbacks')</span></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                     @if(!is_online($model->uid))
                                        @php
                                            $act = activity_time($model->uid);
                                            $lang_info_1 = str_ireplace("%act%",$act,__('crypto.info_1'));
                                            $lang_info_1 = str_ireplace("%process_time%",$model->process_time,$lang_info_1);
                                        @endphp
                                        <div class="alert alert-info"><i class="fa fa-info-circle"></i> {{$lang_info_1}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="panel panel-default panel-blue">
                                <div class="panel-body">
                                    <h3>@lang('crypto.buy_bitcoins')</h3>
                                    <br>
                                    <form action="{{route('account.tradeCreate')}}" method="POST">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="input-group">
                                                  <input type="text" class="form-control input-lg" placeholder="0.00" name="amount" id="amount" onkeyup="calculate_crypto_amount(this.value);" onkeydown="calculate_crypto_amount(this.value);">
                                                  <span class="input-group-addon" id="basic-addon2">{{$model->currency}}</span>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-center"><i class="fa fa-refresh fa-3x"></i></div>
                                            <div class="col-md-5">
                                                <div class="input-group">
                                                  <input type="text" class="form-control input-lg" placeholder="0.0000" name="crypto_amount" id="crypto_amount" onkeyup="calculate_amount(this.value);" onkeydown="calculate_amount(this.value);">
                                                  <span class="input-group-addon" id="basic-addon2">BTC</span>
                                                </div>
                                            </div>
                                            <input type="hidden" name="type" value="sell">
                                            <input type="hidden" name="ad_id" value="{{$model->id}}">
                                            <input type="hidden" name="trader" value="{{$model->uid}}">
                                            <input type="hidden" name="crypto_price" value="{{convertBTCprice($model->price,$model->currency)}}" id="crypto_price">
                                            <div class="col-md-12">
                                                <br>
                                                <center>
                                                    @if($signed_in)
                                                        @if($model->uid == $loginUser->id)
                                                            @lang('crypto.this_ad_is_yours')
                                                        @else
                                                            <button type="submit" class="btn btn-warning btn-lg" name="btc_buy_bitcoins"><i class="cc BTC-alt"></i> @lang('crypto.btn_buy')</button>
                                                        @endif
                                                    @else
                                                        @lang('crypto.login_is_required')
                                                    @endif
                                                </center>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            @if($model->require_document || $model->require_email || $model->require_mobile )
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3>@lang('crypto.this_ad_require')</h3>
                                        @if($model->require_document)
                                            <span class="text text-success"><i class="fa fa-check"></i> @lang('crypto.verified_documents')</span><br/>
                                        @endif
                                        @if($model->require_email)
                                            <span class="text text-success"><i class="fa fa-check"></i> @lang('crypto.verified_email_address')</span><br/>
                                        @endif
                                        @if($model->require_mobile)
                                            <span class="text text-success"><i class="fa fa-check"></i> @lang('crypto.verified_mobile')</span><br/>
                                        @endif
                                        <br/>
                                    </div>
                                </div>
                            @endif
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h3>@lang('crypto.terms_of_trade')</h3>
                                    {{nl2br($model->terms)}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>