-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 05, 2018 at 11:30 PM
-- Server version: 5.6.38
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `crypto`
--

-- --------------------------------------------------------

--
-- Table structure for table `crypto_ads`
--

CREATE TABLE `crypto_ads` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `network` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `payment_instructions` text,
  `price` varchar(255) DEFAULT NULL,
  `min_amount` varchar(255) DEFAULT NULL,
  `max_amount` varchar(255) DEFAULT NULL,
  `process_time` varchar(255) DEFAULT NULL,
  `terms` text,
  `require_document` int(11) DEFAULT NULL,
  `require_email` int(11) DEFAULT NULL,
  `require_mobile` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_ads`
--

INSERT INTO `crypto_ads` (`id`, `uid`, `type`, `network`, `payment_method`, `currency`, `payment_instructions`, `price`, `min_amount`, `max_amount`, `process_time`, `terms`, `require_document`, `require_email`, `require_mobile`, `created_at`, `updated_at`) VALUES
(1, 1, 'buy_bitcoin', 'Bitcoin', 'PayPal', 'USD', 'Payment instructions', '1', '5', '100', '10', 'Terms of trade', 0, 0, 0, '2018-05-05 15:19:57', '0000-00-00 00:00:00'),
(2, 1, 'sell_litecoin', 'Litecoin', 'PayPal', 'USD', 'Payment instructions', '1', '5', '100', '30', 'Terms of trade', 0, 0, 0, '2018-05-05 15:34:02', '0000-00-00 00:00:00'),
(3, 1, 'buy_bitcoin', 'Bitcoin', 'PayPal', 'USD', 'Payment instructions', '1', '5', '100', '30', 'Terms of trade', 0, 0, 0, '2018-05-05 15:53:31', '0000-00-00 00:00:00'),
(4, 1, 'sell_bitcoin', 'Bitcoin', 'PayPal', 'USD', 'Payment instructions', '1', '5', '100', '30', 'Terms of trade', 0, 0, 0, '2018-05-05 15:56:29', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `crypto_blockio_licenses`
--

CREATE TABLE `crypto_blockio_licenses` (
  `id` int(11) NOT NULL,
  `account` varchar(255) DEFAULT NULL,
  `network` varchar(255) DEFAULT NULL,
  `license` varchar(255) DEFAULT NULL,
  `secret_pin` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `addresses` int(11) DEFAULT NULL,
  `default_license` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_blockio_licenses`
--

INSERT INTO `crypto_blockio_licenses` (`id`, `account`, `network`, `license`, `secret_pin`, `address`, `addresses`, `default_license`) VALUES
(1, 'Bitcoin', 'Bitcoin', '2cae-2f28-be1d-2f99', '12345678', '2N56XywkkMVVF6oj9fA7Rb3KH5J6BXWk7xd', 0, 1),
(2, 'Litecoin', 'Litecoin', 'f705-4aad-55d8-4298', '12345678', '2N9TBdoKJ55bNRKy5ZRHGrndAtBkAW9xJ6V', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `crypto_faq`
--

CREATE TABLE `crypto_faq` (
  `id` int(11) NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `answer` text,
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `crypto_pages`
--

CREATE TABLE `crypto_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `content` text,
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_pages`
--

INSERT INTO `crypto_pages` (`id`, `title`, `prefix`, `content`, `created`, `updated`) VALUES
(1, 'Terms of service', 'terms-of-services', 'Edit from WebAdmin.', NULL, NULL),
(2, 'Privacy Policy', 'privacy-policy', 'Edit from WebAdmin.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `crypto_prices`
--

CREATE TABLE `crypto_prices` (
  `id` int(11) NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  `network` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_prices`
--

INSERT INTO `crypto_prices` (`id`, `source`, `network`, `price`, `currency`) VALUES
(1, 'Source', 'Bitcoin', '100', 'USD'),
(2, 'Source', 'Litecoin', '200', 'USD'),
(3, 'Source', 'Dogecoin', '300', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `crypto_settings`
--

CREATE TABLE `crypto_settings` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `infoemail` varchar(255) DEFAULT NULL,
  `supportemail` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `referral_comission` varchar(255) DEFAULT NULL,
  `bitcoin_buy_comission` varchar(255) DEFAULT NULL,
  `bitcoin_sell_comission` varchar(255) DEFAULT NULL,
  `litecoin_buy_comission` varchar(255) DEFAULT NULL,
  `litecoin_sell_comission` varchar(255) DEFAULT NULL,
  `dogecoin_buy_comission` varchar(255) DEFAULT NULL,
  `dogecoin_sell_comission` varchar(255) DEFAULT NULL,
  `bitcoin_withdrawal_comission` varchar(255) DEFAULT NULL,
  `litecoin_withdrawal_comission` varchar(255) DEFAULT NULL,
  `dogecoin_withdrawal_comission` varchar(255) DEFAULT NULL,
  `profits` varchar(255) DEFAULT NULL,
  `document_verification` int(11) DEFAULT NULL,
  `email_verification` int(11) DEFAULT NULL,
  `phone_verification` int(11) DEFAULT NULL,
  `recaptcha_verification` int(11) DEFAULT NULL,
  `recaptcha_publickey` varchar(255) DEFAULT NULL,
  `recaptcha_privatekey` varchar(255) DEFAULT NULL,
  `bitcoin_status` int(11) DEFAULT NULL,
  `litecoin_status` int(11) DEFAULT NULL,
  `dogecoin_status` int(11) DEFAULT NULL,
  `nexmo_api_key` varchar(255) DEFAULT NULL,
  `nexmo_api_secret` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_settings`
--

INSERT INTO `crypto_settings` (`id`, `title`, `description`, `keywords`, `name`, `infoemail`, `supportemail`, `url`, `referral_comission`, `bitcoin_buy_comission`, `bitcoin_sell_comission`, `litecoin_buy_comission`, `litecoin_sell_comission`, `dogecoin_buy_comission`, `dogecoin_sell_comission`, `bitcoin_withdrawal_comission`, `litecoin_withdrawal_comission`, `dogecoin_withdrawal_comission`, `profits`, `document_verification`, `email_verification`, `phone_verification`, `recaptcha_verification`, `recaptcha_publickey`, `recaptcha_privatekey`, `bitcoin_status`, `litecoin_status`, `dogecoin_status`, `nexmo_api_key`, `nexmo_api_secret`) VALUES
(1, 'Crypto', 'this is description', 'this is keywords', 'Crypto', 'info@gmail.com', 'support@gmail.com', 'http://localhost/cryptomarket/client_script/', NULL, '2', '2', '2', '2', '2', '2', '2', '2', '2', NULL, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, '84288059', 'BB3kHSdS6vdrRA5c');

-- --------------------------------------------------------

--
-- Table structure for table `crypto_sms_codes`
--

CREATE TABLE `crypto_sms_codes` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `sms_code` varchar(255) DEFAULT NULL,
  `verified` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `crypto_trades`
--

CREATE TABLE `crypto_trades` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `network` varchar(255) DEFAULT NULL,
  `ad_id` int(11) DEFAULT NULL,
  `trader` int(11) DEFAULT NULL,
  `payment_hash` varchar(255) DEFAULT NULL,
  `crypto_price` varchar(255) DEFAULT NULL,
  `crypto_amount` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `payment_instructions` text,
  `status` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL,
  `released_bitcoins` int(11) NOT NULL DEFAULT '0',
  `referral_id` int(11) DEFAULT NULL,
  `attachment` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_trades`
--

INSERT INTO `crypto_trades` (`id`, `uid`, `type`, `network`, `ad_id`, `trader`, `payment_hash`, `crypto_price`, `crypto_amount`, `amount`, `payment_instructions`, `status`, `created`, `timeout`, `released_bitcoins`, `referral_id`, `attachment`) VALUES
(1, 2, 'buy_bitcoin', 'Bitcoin', 4, 1, 'BA8CC8C3F8', '99.00', '0.050505', '5', 'Payment instructions', 4, 1525536103, 1525537903, 0, NULL, NULL),
(2, 2, 'buy_bitcoin', 'Bitcoin', 4, 1, '7EA85226FF', '99.00', '0.050505', '5', 'Payment instructions', 5, 1525536336, 1525538136, 0, NULL, NULL),
(3, 2, 'buy_bitcoin', 'Bitcoin', 4, 1, '2643CA8798', '99.00', '0.050505', '5', 'Payment instructions', 7, 1525542947, 1525544747, 1, NULL, NULL),
(4, 2, 'buy_bitcoin', 'Bitcoin', 4, 1, 'C30AC5B73A', '99.00', '0.050505', '5', 'Payment instructions', 5, 1525545420, 1525547220, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `crypto_trades_messages`
--

CREATE TABLE `crypto_trades_messages` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `trade_id` int(11) DEFAULT NULL,
  `message` text,
  `attachment` int(11) NOT NULL DEFAULT '0',
  `readed` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_trades_messages`
--

INSERT INTO `crypto_trades_messages` (`id`, `uid`, `trade_id`, `message`, `attachment`, `readed`, `time`) VALUES
(1, 2, 1, 'this is message', 0, 0, 1525536209),
(2, 1, 2, 'sdf', 0, 1, 1525542545),
(3, 2, 2, 'asfd', 0, 1, 1525542562),
(4, 2, 3, 'test chat', 0, 1, 1525543066),
(5, 1, 3, 'another reply', 0, 1, 1525543076);

-- --------------------------------------------------------

--
-- Table structure for table `crypto_trades_reports`
--

CREATE TABLE `crypto_trades_reports` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `trade_id` int(11) DEFAULT NULL,
  `content` text,
  `status` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `crypto_users`
--

CREATE TABLE `crypto_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `secret_pin` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_verified` int(11) DEFAULT NULL,
  `email_hash` text,
  `document_verified` int(11) DEFAULT NULL,
  `document_1` text,
  `document_2` text,
  `mobile_verified` int(11) DEFAULT NULL,
  `mobile_number` text,
  `status` varchar(255) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `time_signup` int(11) DEFAULT NULL,
  `time_signin` int(11) DEFAULT NULL,
  `time_activity` int(11) DEFAULT NULL,
  `referral_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_users`
--

INSERT INTO `crypto_users` (`id`, `username`, `password`, `secret_pin`, `email`, `email_verified`, `email_hash`, `document_verified`, `document_1`, `document_2`, `mobile_verified`, `mobile_number`, `status`, `hash`, `ip`, `time_signup`, `time_signin`, `time_activity`, `referral_id`) VALUES
(1, 'cotrade1', 'f5bb0c8de146c67b44babbf4e6584cc0', '1234', 'otr1@gmail.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, '3', NULL, '::1', 1525532572, 1525532697, 1525553024, 0),
(2, 'cotrader2', 'f5bb0c8de146c67b44babbf4e6584cc0', '1234', 'otr2@gmail.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, '3', NULL, '127.0.0.1', 1525534353, 1525534363, 1525550763, 0);

-- --------------------------------------------------------

--
-- Table structure for table `crypto_users_addresses`
--

CREATE TABLE `crypto_users_addresses` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `network` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `lid` int(11) DEFAULT NULL,
  `available_balance` varchar(255) DEFAULT NULL,
  `pending_received_balance` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_users_addresses`
--

INSERT INTO `crypto_users_addresses` (`id`, `uid`, `network`, `label`, `address`, `lid`, `available_balance`, `pending_received_balance`, `status`, `created`, `updated`) VALUES
(1, 1, 'Bitcoin', 'usr_cotrade1', '2NAXwyRWqLA2WgdScWNdvcb2UgryvBEhH4D', 1, '5.00000000', '0.00000000', '1', 1525532573, NULL),
(2, 2, 'Bitcoin', 'usr_cotrader2', '2MsKhN9TNzsLJJpskXbNX4cAUwdyLuoGRTG', 1, '10.00000000', '0.00000000', '1', 1525534354, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `crypto_users_notifications`
--

CREATE TABLE `crypto_users_notifications` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `notified` int(11) DEFAULT NULL,
  `trade_id` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_users_notifications`
--

INSERT INTO `crypto_users_notifications` (`id`, `uid`, `notified`, `trade_id`, `time`) VALUES
(1, 1, 1, 1, 1525536103),
(2, 1, 1, 2, 1525536336),
(3, 1, 1, 3, 1525542947),
(4, 1, 1, 4, 1525545420);

-- --------------------------------------------------------

--
-- Table structure for table `crypto_users_ratings`
--

CREATE TABLE `crypto_users_ratings` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `trade_id` int(11) DEFAULT NULL,
  `comment` text,
  `author` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crypto_users_ratings`
--

INSERT INTO `crypto_users_ratings` (`id`, `uid`, `type`, `trade_id`, `comment`, `author`, `time`) VALUES
(1, 1, 3, 3, 'asdasdfasdfas', 2, 1525550763),
(2, 2, 1, 3, 'asdfas', 1, 1525551901);

-- --------------------------------------------------------

--
-- Table structure for table `crypto_users_transactions`
--

CREATE TABLE `crypto_users_transactions` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `network` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) DEFAULT NULL,
  `sender` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `confirmations` int(11) DEFAULT NULL,
  `txid` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crypto_ads`
--
ALTER TABLE `crypto_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_blockio_licenses`
--
ALTER TABLE `crypto_blockio_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_faq`
--
ALTER TABLE `crypto_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_pages`
--
ALTER TABLE `crypto_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_prices`
--
ALTER TABLE `crypto_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_settings`
--
ALTER TABLE `crypto_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_sms_codes`
--
ALTER TABLE `crypto_sms_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_trades`
--
ALTER TABLE `crypto_trades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_trades_messages`
--
ALTER TABLE `crypto_trades_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_trades_reports`
--
ALTER TABLE `crypto_trades_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_users`
--
ALTER TABLE `crypto_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_users_addresses`
--
ALTER TABLE `crypto_users_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_users_notifications`
--
ALTER TABLE `crypto_users_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_users_ratings`
--
ALTER TABLE `crypto_users_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crypto_users_transactions`
--
ALTER TABLE `crypto_users_transactions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crypto_ads`
--
ALTER TABLE `crypto_ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `crypto_blockio_licenses`
--
ALTER TABLE `crypto_blockio_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `crypto_faq`
--
ALTER TABLE `crypto_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crypto_pages`
--
ALTER TABLE `crypto_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `crypto_prices`
--
ALTER TABLE `crypto_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `crypto_settings`
--
ALTER TABLE `crypto_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `crypto_sms_codes`
--
ALTER TABLE `crypto_sms_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crypto_trades`
--
ALTER TABLE `crypto_trades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `crypto_trades_messages`
--
ALTER TABLE `crypto_trades_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `crypto_trades_reports`
--
ALTER TABLE `crypto_trades_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crypto_users`
--
ALTER TABLE `crypto_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `crypto_users_addresses`
--
ALTER TABLE `crypto_users_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `crypto_users_notifications`
--
ALTER TABLE `crypto_users_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `crypto_users_ratings`
--
ALTER TABLE `crypto_users_ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `crypto_users_transactions`
--
ALTER TABLE `crypto_users_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
